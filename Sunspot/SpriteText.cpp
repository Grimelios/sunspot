#include "SpriteText.h"
#include <utility>
#include "ContentCache.h"
#include "GameFunctions.h"

namespace Sunspot
{
	SpriteText::SpriteText(const std::string& font, const std::optional<std::string>& value, const Alignments alignment) :
		SpriteText(ContentCache::GetFont(font), value, alignment)
	{
	}

	SpriteText::SpriteText(const SpriteFont& font, std::optional<std::string> value, const Alignments alignment) :
		Component2D(alignment),

		font(font),
		value(std::move(value))
	{
	}

	int SpriteText::Size() const
	{
		return font.Size();
	}

	const std::optional<std::string>& SpriteText::Value() const
	{
		return value;
	}

	void SpriteText::Value(std::optional<std::string> value)
	{
		this->value = std::move(value);

		if (this->value.has_value())
		{
			const glm::ivec2 dimensions = font.Measure(this->value.value());

			origin = GameFunctions::ComputeOrigin(dimensions.x, dimensions.y, alignment);
		}
	}

	void SpriteText::Clear()
	{
		Value(std::nullopt);
	}

	void SpriteText::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		if (value.has_value())
		{
			sb.DrawString(font, value.value(), position, origin, rotation, color, scale);
		}
	}
}
