#pragma once
#include <nlohmann/json.hpp>

namespace Sunspot::JsonUtilities
{
	nlohmann::json Load(const std::string& filename);
}
