#include "DirectionalHurtbox.h"

namespace Sunspot
{
	DirectionalHurtbox::DirectionalHurtbox(Shape* shape, const glm::vec2& direction, DamageSource* source) :
		Hurtbox(shape, source),

		direction(direction)
	{
	}

	void DirectionalHurtbox::ApplyHit(const int damage, const int knockback, ITargetable* target)
	{
		target->RegisterHit(damage, knockback, direction, source->Parent());
	}
}
