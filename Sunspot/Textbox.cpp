#include <glad.h>
#include "Textbox.h"
#include <locale>

namespace Sunspot
{
	Textbox::NumericSpecials Textbox::numericSpecials = 
	{
		')', '!', '@', '#', '$', '%', '^', '&', '*', '('
	};

	void Textbox::HandleKeyboard(const KeyboardData& data)
	{
		for (const KeyPress& keyPress : data.KeysPressedThisFrame())
		{
			const int key = keyPress.Key();

			switch (key)
			{
				case GLFW_KEY_BACKSPACE:
					if (!value.empty() && cursor > 0)
					{
						value.erase(cursor - 1, 1);
						cursor--;
					}

					break;

				case GLFW_KEY_DELETE:
					if (!value.empty() && cursor < static_cast<int>(value.size()))
					{
						value.erase(cursor, 1);
					}

					break;

				default:
					const bool shift = (keyPress.Mods() & GLFW_MOD_SHIFT) > 0;
					const bool capsLock = false;

					const std::optional<char> c = GetCharacter(key, shift, capsLock);

					if (c.has_value())
					{
						value.insert(cursor, 1, c.value());
						cursor++;
					}

					break;
			}
		}
	}

	std::optional<char> Textbox::GetCharacter(const int key, bool shift, const bool capsLock)
	{
		// See http://www.glfw.org/docs/latest/group__keys.html.
		if (key >= GLFW_KEY_A && key <= GLFW_KEY_Z)
		{
			const char c = static_cast<char>(key);

			if (capsLock)
			{
				shift = !shift;
			}

			return shift ? c : std::tolower(c, std::locale());
		}

		if (key >= GLFW_KEY_0 && key <= GLFW_KEY_9)
		{
			return shift ? numericSpecials[key - GLFW_KEY_0] : static_cast<char>(key);
		}

		// Similar to below, numpad functions are handled before this function is called.
		if (key >= GLFW_KEY_KP_0 && key <= GLFW_KEY_KP_9)
		{
			return static_cast<char>(key - (GLFW_KEY_KP_0 - GLFW_KEY_0));
		}

		switch (key)
		{
			case GLFW_KEY_COMMA: return shift ? '<' : ',';
			case GLFW_KEY_PERIOD: return shift ? '>' : '.';
			case GLFW_KEY_SLASH: return shift ? '?' : '/';
			case GLFW_KEY_SEMICOLON: return shift ? ':' : ';';
			case GLFW_KEY_APOSTROPHE: return shift ? '"' : '\'';
			case GLFW_KEY_LEFT_BRACKET: return shift ? '{' : '[';
			case GLFW_KEY_RIGHT_BRACKET: return shift ? '}' : ']';
			case GLFW_KEY_BACKSLASH: return shift ? '|' : '\\';
			case GLFW_KEY_MINUS: return shift ? '_' : '-';
			case GLFW_KEY_EQUAL: return shift ? '+' : '=';
			case GLFW_KEY_GRAVE_ACCENT: return shift ? '~' : '`';

			// Numlock keypad functions are handled before this function is called.
			case GLFW_KEY_KP_ADD: return '+';
			case GLFW_KEY_KP_SUBTRACT: return '-';
			case GLFW_KEY_KP_MULTIPLY: return '*';
			case GLFW_KEY_KP_DIVIDE: return '/';
			case GLFW_KEY_KP_DECIMAL: return '.';
		}

		return std::nullopt;
	}
}
