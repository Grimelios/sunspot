#pragma once
#include "Button.h"

namespace Sunspot
{
	class ArrowButton : public Button
	{
	public:

		ArrowButton(const Texture2D& texture, const Bounds& source, bool flipped = false);

		void OnHover() override;
		void OnUnhover() override;
	};
}
