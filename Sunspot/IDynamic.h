#pragma once

namespace Sunspot
{
	class IDynamic
	{
	public:

		virtual ~IDynamic() = default;
		virtual void Update(float dt) = 0;
	};
}
