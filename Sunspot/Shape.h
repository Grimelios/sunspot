#pragma once
#include <glm/vec2.hpp>

namespace Sunspot
{
	enum class ShapeTypes
	{
		AxisBox,
		Box,
		Circle,
		Line,
		Sector
	};

	class Shape
	{
	private:

		ShapeTypes shapeType;

	protected:

		Shape(const glm::vec2& position, float rotation, ShapeTypes type);

	public:

		virtual ~Shape() = 0;

		ShapeTypes ShapeType() const;

		// Not all shapes have position and rotation, but it's simpler to add them to the base class anyway (since most
		// shapes do).
		glm::vec2 position;

		float rotation;
	};

	inline Shape::~Shape() = default;
}
