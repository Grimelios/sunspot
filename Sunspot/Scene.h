#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <vector>
#include <map>
#include "DoublyLinkedList.h"
#include <nlohmann/json.hpp>
#include "EntityHandle.h"
#include "FragmentLoader.h"

namespace Sunspot
{
	class Entity;
	class Scene : public IDynamic, public IRenderable
	{
	private:

		using EntityPointer = std::unique_ptr<Entity>;
		using EntityList = DoublyLinkedList<EntityPointer>;
		using EntityNode = DoublyLinkedListNode<EntityPointer>;
		using HandleMap = std::map<EntityTypes, std::vector<EntityHandle*>>;

		// Note that this list of handles is temporary. Handles are added to this list when an entity registers itself
		// before another classes requests that handle, such that the registered entity can be passed through later on.
		std::vector<EntityHandle> handleList;

		EntityList entities;
		HandleMap handleMap;
		FragmentLoader fragmentLoader;

		EntityHandle*& VerifyHandles(EntityTypes type, int id);

		// This function is used for both fragment loading and on-the-fly entity creation. Note that the return value
		// isn't used for fragment loading.
		Entity* AddInternal(EntityPointer& e);

	public:

		// Note that this function isn't used when loading fragments from Json files. It's only used for on-the-fly
		// entity creation.
		template<class T, class... Args>
		T* Add(Args&&... args);

		void LoadFragment(const std::string& filename);
		void RegisterHandle(EntityTypes type, void* entity, int id);
		void GetHandle(EntityTypes type, int id, EntityHandle& handle);
		void Remove(Entity* entity);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};

	template<class T, class...Args>
	T* Scene::Add(Args&&... args)
	{
		// Nodes store unique pointers to generic entities. As such, the pointer passed into the node constructor must
		// be explicitly marked as std::unique_ptr<Entity> rather than std::unique_ptr<T>.
		EntityPointer e = std::make_unique<T>(args...);

		return static_cast<T*>(AddInternal(e));
	}
}
