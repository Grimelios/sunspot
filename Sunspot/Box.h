#pragma once
#include "Shape.h"

namespace Sunspot
{
	class Box : public Shape
	{
	public:

		Box();
		Box(int width, int height);
		Box(int width, int height, const glm::vec2& center, float rotation);

		int width;
		int height;
	};
}
