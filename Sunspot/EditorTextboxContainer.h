#pragma once
#include "TextboxContainer.h"
#include "SpriteText.h"

namespace Sunspot
{
	class EditorTextboxContainer : public TextboxContainer
	{
	private:

		SpriteText spriteText;

	public:

		EditorTextboxContainer();

		void Clear() override;
		void Cursor(int cursor) override;
		void Location(const glm::ivec2& location) override;
		void Value(const std::string& value) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
