#pragma once
#include "Entity.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	class RunController : public IDynamic
	{
	private:

		Entity* parent;

		int maxSpeed = 0;
		int maxSpeedSquared = 0;

	public:

		explicit RunController(Entity* parent);

		int acceleration = 0;
		int deceleration = 0;

		glm::vec2 direction = glm::vec2(0);

		void MaxSpeed(int maxSpeed);
		void Update(float dt) override;
	};
}
