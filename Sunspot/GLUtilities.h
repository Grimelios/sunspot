#pragma once
#include <glad.h>
#include <vector>

namespace Sunspot::GLUtilities
{
	template<class T>
	void BufferData(const GLuint buffer, std::vector<T> data, const int stride)
	{
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glBufferData(GL_ARRAY_BUFFER, data.size() * stride, &data[0], GL_STATIC_DRAW);
	}

	GLuint CreateTarget(int width, int height);
}
