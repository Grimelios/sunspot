#pragma once
#include "LivingEntity.h"

namespace Sunspot
{
	class BigTentacle : public LivingEntity
	{
	protected:

		void OnDeath() override;

	public:

		explicit BigTentacle(const Json& j);
	};
}
