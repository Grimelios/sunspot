#pragma once
#include "DataClass.h"

namespace Sunspot
{
	class ShipWheelData : public DataClass
	{
	private:

		float rotation;

		int time;
		int sensorWidth;
		int sensorHeight;
		int sensorOffset;

	public:

		ShipWheelData();

		float Rotation() const;

		int Time() const;
		int SensorWidth() const;
		int SensorHeight() const;
		int SensorOffset() const;
	};
}
