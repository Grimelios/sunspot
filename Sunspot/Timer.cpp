#include "Timer.h"
#include <utility>

namespace Sunspot
{
	Timer::Timer(const int duration, const NFunction& trigger, const float elapsed) :
		Timer(duration, nullptr, trigger, elapsed)
	{
	}

	Timer::Timer(const int duration, Tick tick, NFunction trigger, const float elapsed) :
		tick(std::move(tick)),
		nFunction(std::move(trigger)),
		repeating(false),
		elapsed(elapsed),
		duration(duration / 1000.0f)
	{
	}

	Timer::Timer(const int duration, const RFunction& trigger, const float elapsed) :
		Timer(duration, nullptr, trigger, elapsed)
	{
	}

	Timer::Timer(const int duration, Tick tick, RFunction trigger, const float elapsed) :
		tick(std::move(tick)),
		rFunction(std::move(trigger)),
		repeating(true),
		elapsed(elapsed),
		duration(duration / 1000.0f)
	{
	}

	bool Timer::Completed() const
	{
		return completed;
	}

	void Timer::Update(const float dt)
	{
		// It's assumed that the timer is unpaused if this function is called (meaning that external classes should handle
		// pausing).
		elapsed += dt;

		if (repeating)
		{
			// A loop is required for durations less than a frame. It's also possible for a timer to pause itself during a
			// repeating function.
			while (elapsed >= duration && !paused)
			{
				// It's possible for a repeating function to change the timer's duration.
				const float previousDuration = duration;

				// Returning false from a repeating function means that the timer should end.
				if (!rFunction(std::fmod(elapsed, duration)))
				{
					completed = true;

					return;
				}

				elapsed -= previousDuration;
			}
		}
		else if (elapsed >= duration)
		{
			nFunction(elapsed - duration);
			completed = true;

			return;
		}

		if (tick != nullptr)
		{
			tick(elapsed / duration);
		}
	}
}
