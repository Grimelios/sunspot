#pragma once
#include "Game.h"
#include <memory>
#include "GameLoop.h"
#include "SpriteBatch.h"

namespace Sunspot
{
	class SunspotGame : public Game
	{
	private:

		SpriteBatch sb;
		PrimitiveBatch pb;

		std::unique_ptr<GameLoop> gameLoop = nullptr;

	protected:

		void Update(float dt) override;
		void Draw() override;

	public:

		SunspotGame(int width, int height);

		void Initialize() override;
	};
}
