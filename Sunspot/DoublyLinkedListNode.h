#pragma once

namespace Sunspot
{
	template<class T>
	class DoublyLinkedListNode
	{
	public:

		template<class... Args>
		explicit DoublyLinkedListNode(Args&&... args);
		explicit DoublyLinkedListNode(T& data);

		T data;

		DoublyLinkedListNode<T>* next = nullptr;
		DoublyLinkedListNode<T>* previous = nullptr;
	};

	template <class T>
	template <class... Args>
	DoublyLinkedListNode<T>::DoublyLinkedListNode(Args&&... args) : data(args...)
	{
	}

	template <class T>
	DoublyLinkedListNode<T>::DoublyLinkedListNode(T& data) : data(std::move(data))
	{
	}
}
