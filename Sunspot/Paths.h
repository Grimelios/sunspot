#pragma once
#include <string>

namespace Sunspot::Paths
{
	const std::string Fonts = "Content/Fonts/";
	const std::string Json = "Content/Json/";
	const std::string Textures = "Content/Textures/";
	const std::string Shaders = "Content/Shaders/";
}
