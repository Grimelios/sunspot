#pragma once
#include "Texture2D.h"
#include <string>
#include "Paths.h"
#include <map>
#include "SpriteFont.h"
#include "FontLoader.h"

namespace Sunspot
{
	class ContentCache
	{
	private:

		using FontCache = std::map<std::string, SpriteFont>;
		using TextureCache = std::map<std::string, Texture2D>;

		static FontCache fontCache;
		static FontLoader fontLoader;
		static TextureCache textureCache;

	public:

		static const SpriteFont& GetFont(const std::string& name, bool useKerning = true);
		static const Texture2D& GetTexture(const std::string& filename, const std::string& folder = Paths::Textures);
	};
}
