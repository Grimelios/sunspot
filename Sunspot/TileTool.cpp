#include "TileTool.h"
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include "GameFunctions.h"
#include <glad.h>
#include <glfw3.h>
#include "TilePanel.h"

namespace Sunspot
{
	TileTool::TileTool() : EditorTool("Tiles"),
		bounds(1, 1)
	{
		panel = std::make_unique<TilePanel>();
	}

	void TileTool::Reload(Scene& scene)
	{
		//tilemaps = scene.GetLayer(0).GetList<Tilemap>(EntityGroups::World);
	}

	void TileTool::HandleMouse(const MouseData& data)
	{
		const glm::vec2& mousePosition = data.WorldPosition();

		// Simpler to just reset and re-check tilemaps every frame.
		hoveredMap = nullptr;

		for (Tilemap* tilemap : tilemaps)
		{
			if (tilemap->GetRenderBounds().Contains(mousePosition))
			{
				hoveredMap = static_cast<Tilemap*>(tilemap);
			}
		}

		glm::ivec2 tileCoords;
		tileCoords.x = static_cast<int>(std::floor(mousePosition.x / tileSize));
		tileCoords.y = static_cast<int>(std::floor(mousePosition.y / tileSize));

		const bool leftClick = data.Query(GLFW_MOUSE_BUTTON_LEFT, InputStates::PressedThisFrame);
		const bool rightClick = data.Query(GLFW_MOUSE_BUTTON_RIGHT, InputStates::PressedThisFrame);

		if (creationActive)
		{
			if (leftClick)
			{
			}
			else if (rightClick)
			{
				creationActive = false;
				bounds.SetLocation(tileCoords);
				bounds.width = 1;
				bounds.height = 1;

				return;
			}

			const int x = std::min(anchor.x, tileCoords.x);
			const int y = std::min(anchor.y, tileCoords.y);
			const int width = std::max(anchor.x, tileCoords.x) - x + 1;
			const int height = std::max(anchor.y, tileCoords.y) - y + 1;

			bounds.x = x;
			bounds.y = y;
			bounds.width = width;
			bounds.height = height;

			statusBar->Append("Bounds: " + GameFunctions::ToString(bounds));
		}
		else
		{
			bounds.SetLocation(tileCoords);

			if (leftClick)
			{
				creationActive = true;
				anchor = tileCoords;
			}
		}

		// This condition is separate so that the status updates if creation mode is canceled.
		if (!creationActive)
		{
			statusBar->Append("Tile: " + GameFunctions::ToString(tileCoords));
		}
	}

	void TileTool::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		pb.DrawBounds(bounds.Scale(tileSize), glm::vec4(1));

		if (hoveredMap != nullptr)
		{
			pb.DrawBounds(hoveredMap->GetRenderBounds(), glm::vec4(1));
		}
	}
}
