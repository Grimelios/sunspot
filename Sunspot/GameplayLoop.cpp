#include "GameplayLoop.h"
#include "Resolution.h"
#include "Enemy.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	GameplayLoop::GameplayLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch& pb) :
		GameLoop(camera, sb, pb),

		editor(camera),
		shapeVisualizer(sensorWorld),
		curveVisualizer(curve)
	{
		gameplayBounds = editor.GetGameplayBounds();
		gameplayTarget = RenderTarget(gameplayBounds.width, gameplayBounds.height);

		Sensor::world = &sensorWorld;
		Resolution::Target = gameplayBounds;
	}

	void GameplayLoop::Initialize()
	{
		scene.LoadFragment("Shipwreck01.json");
		scene.Add<Player>();
		editor.Reload(scene);

		std::vector<glm::vec2>& controlPoints = curve.controlPoints;
		controlPoints.emplace_back(0, -280);
		controlPoints.emplace_back(-450, -140);
		controlPoints.emplace_back(250, 0);
		controlPoints.emplace_back(-300, 140);
		controlPoints.emplace_back(-20, 280);
	}

	void GameplayLoop::Update(const float dt)
	{
		sensorWorld.Update(dt);
		scene.Update(dt);
	}

	void GameplayLoop::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		if (editorActive)
		{
			editor.Draw(sb, pb);

			sb.SetTarget(&gameplayTarget);
			sb.Begin(BatchCoords::World);
			pb.Begin(BatchCoords::World);

			scene.Draw(sb, pb);

			if (editor.MouseWithinGameplayBounds())
			{
				editor.ActiveTool()->Draw(sb, pb);
			}

			shapeVisualizer.Draw(pb);
			curveVisualizer.Draw(pb);

			pb.End();
			sb.End();
			sb.SetTarget(nullptr);
			sb.Begin(BatchCoords::Screen);
			sb.Draw(gameplayTarget, gameplayBounds.GetLocation());

			headsUpDisplay.Draw(sb, pb);

			sb.End();
		}
	}
}
