#pragma once
#include <string>
#include <vector>

namespace Sunspot::StringUtilities
{
	std::vector<std::string> Split(const std::string& s, char delimeter, bool removeEmpty = false);
	
	int IndexOf(const std::string& s, char c);
	int IndexOf(const std::string& s, char c, int start);
	int IndexOf(const std::string& s, const std::string& value);
	int IndexOf(const std::string& s, const std::string& value, int start);
	int LastIndexOf(const std::string& s, char c);
}
