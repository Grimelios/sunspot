#pragma once
#include "GameLoop.h"
#include "Editor.h"
#include "RenderTarget.h"
#include "Scene.h"
#include "SensorWorld.h"
#include "ShapeVisualizer.h"
#include "Curve.h"
#include "CurveVisualizer.h"
#include "HeadsUpDisplay.h"

namespace Sunspot
{
	class GameplayLoop : public GameLoop
	{
	private:

		Scene scene;
		Editor editor;
		Bounds gameplayBounds;
		SensorWorld sensorWorld;
		RenderTarget gameplayTarget;
		HeadsUpDisplay headsUpDisplay;
		ShapeVisualizer shapeVisualizer;

		Curve curve;
		CurveVisualizer curveVisualizer;

		bool editorActive = true;

	public:

		GameplayLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch& pb);

		void Initialize() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
