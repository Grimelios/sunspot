#include "Player.h"
#include "Messaging.h"
#include "GameFunctions.h"
#include "PlayerData.h"
#include "AxisBox.h"
#include "MouseData.h"

namespace Sunspot
{
	Player::Player() :
		sword(this),
		sprite("Player.png"),
		runController(this)
	{
		const PlayerData data;

		const int width = data.sourceWidth;
		const int height = data.sourceHeight;

		CreateSensor<AxisBox>(SensorTypes::Player, width, height);

		sensor->canActivate.push_back(SensorTypes::Mechanism);
		sprite.Source(Bounds(0, 0, width, height));
		components.push_back(&sprite);
		runController.MaxSpeed(200);

		Messaging::Subscribe(MessageTypes::Input, [this](const std::any& data, const float dt)
		{
			HandleInput(std::any_cast<AggregateData>(data), dt);
		});
	}

	void Player::HandleInput(const AggregateData& data, const float dt)
	{
		const bool attack = data.Query(controls.attack, InputStates::PressedThisFrame);

		if (attack && !sword.HurtboxActive())
		{
			const MouseData& mouseData = data.Data<MouseData>(InputTypes::Mouse);
			const glm::vec2 attackDirection = glm::normalize(mouseData.WorldPosition() - position);

			sword.TriggerAttack(attackDirection, this);
		}

		const bool up = data.Query(controls.up, InputStates::Held);
		const bool down = data.Query(controls.down, InputStates::Held);
		const bool left = data.Query(controls.left, InputStates::Held);
		const bool right = data.Query(controls.right, InputStates::Held);
		const bool horizontal = left ^ right;
		const bool vertical = up ^ down;

		glm::vec2 direction = glm::vec2(0);

		if (horizontal)
		{
			direction.x = static_cast<float>(left ? -1 : 1);
		}

		if (vertical)
		{
			direction.y = static_cast<float>(up ? -1 : 1);
		}

		if (horizontal && vertical)
		{
			direction = normalize(direction);
		}

		runController.direction = direction;
		runController.Update(dt);
	}

	void Player::Update(const float dt)
	{
		Entity::Update(dt);

		if (sword.HurtboxActive())
		{
			sword.ActiveShape()->position = position;
		}

		sword.Update(dt);
	}
}
