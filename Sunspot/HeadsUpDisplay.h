#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <array>
#include <map>
#include <memory>
#include <glm/vec2.hpp>
#include "HudElement.h"
#include <nlohmann/json.hpp>

namespace Sunspot
{
	class HeadsUpDisplay : public IDynamic, public IRenderable
	{
	private:

		using Json = nlohmann::json;
		using ElementPointer = std::unique_ptr<HudElement>;
		using ElementArray = std::array<ElementPointer, static_cast<int>(HudElementTypes::Count)>;
		using ElementMap = std::map<std::string, HudElementTypes>;

		// The assumption in passing the texture parameter is that all elements will use the same source texture (one
		// big spritesheet).
		static ElementPointer CreateElement(HudElementTypes type, const Texture2D& texture);

		// Note that all elements are assumed to be valid at all times during gameplay (even if some elements are
		// hidden).
		ElementArray elements;

		// Dimensions aren't required on resize since target bounds are used rather than the full window (although
		// they'll be the same during normal gameplay).
		void OnResize();

	public:

		HeadsUpDisplay();

		template<class T>
		T* GetElement(HudElementTypes type);

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};

	template<class T>
	T* HeadsUpDisplay::GetElement(const HudElementTypes type)
	{
		return static_cast<T*>(elements[static_cast<int>(type)].get());
	}
}

