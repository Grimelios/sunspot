#include "HealthDisplay.h"

namespace Sunspot
{
	HealthDisplay::HealthDisplay(const Texture2D& texture) :
		sprite(texture, Alignments::Right | Alignments::Top)
	{
	}

	void HealthDisplay::SetLocation(const glm::ivec2& location)
	{
		sprite.position = location;
	}

	void HealthDisplay::Update(const float dt)
	{
	}

	void HealthDisplay::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		sprite.Draw(sb, pb);
	}
}
