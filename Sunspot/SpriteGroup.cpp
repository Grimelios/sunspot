#include "SpriteGroup.h"

namespace Sunspot
{
	void SpriteGroup::Clear()
	{
		vertices.clear();
		texCoords.clear();
		quadCoords.clear();
		colors.clear();
		indices.clear();

		indexOffset = 0;
	}
}
