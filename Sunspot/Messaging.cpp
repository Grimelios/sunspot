#include "Messaging.h"
#include "VectorUtilities.h"

namespace Sunspot
{
	Messaging::ReceiverArray Messaging::receiverArray;
	Messaging::IndexArray Messaging::indexArray;

	void Messaging::Initialize()
	{
		for (int i = 0; i < TypeCount; i++)
		{
			indexArray[i] = 0;
		}
	}

	int Messaging::Subscribe(const MessageTypes messageType, const ReceiverFunction& action)
	{
		const int typeIndex = static_cast<int>(messageType);
		unsigned int positionIndex = indexArray[typeIndex];

		ReceiverVector& v = receiverArray[typeIndex];

		if (positionIndex == v.size())
		{
			v.emplace_back(action);
			indexArray[typeIndex]++;
		}
		else
		{
			v[positionIndex] = action;

			do
			{
				positionIndex++;
			}
			while (positionIndex < v.size() && v[positionIndex].has_value());

			indexArray[typeIndex] = positionIndex;
		}

		return indexArray[typeIndex];
	}

	void Messaging::Unsubscribe(const MessageTypes messageType, const int index)
	{
		const int typeIndex = static_cast<int>(messageType);
		const int positionIndex = indexArray[typeIndex];

		VectorUtilities::RemoveAt(receiverArray[typeIndex], index);

		indexArray[typeIndex] = std::min(positionIndex, index);
	}

	void Messaging::Send(MessageTypes messageType, const std::any& data, const float dt)
	{
		const ReceiverVector& v = receiverArray[static_cast<int>(messageType)];

		for (unsigned int i = 0; i < v.size(); i++)
		{
			const std::optional<ReceiverFunction>& r = v[i];

			if (r.has_value())
			{
				r.value()(data, dt);
			}
		}
	}

	void Messaging::ProcessChanges()
	{
	}
}
