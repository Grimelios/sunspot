#include "BigTentacle.h"
#include "AxisBox.h"

namespace Sunspot
{
	BigTentacle::BigTentacle(const Json& j)
	{
		CreateSensor<AxisBox>(SensorTypes::Boss, 100, 500);
	}

	void BigTentacle::OnDeath()
	{
	}
}
