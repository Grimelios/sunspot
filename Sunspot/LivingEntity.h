#pragma once
#include "Entity.h"
#include "ITargetable.h"

namespace Sunspot
{
	class LivingEntity : public Entity, public ITargetable
	{
	protected:

		int health = 0;
		int maxHealth = 0;

		virtual void OnDeath();

	public:

		virtual ~LivingEntity() = 0;

		void RegisterHit(int damage, int knockback, const glm::vec2& direction, Entity* source) override;
		void Kill();
	};

	inline LivingEntity::~LivingEntity() = default;
}
