#pragma once
#include "Entity.h"

namespace Sunspot
{
	class Player;

	// Mechanisms are activated entities that trigger some change in the world. Many mechanisms store handles to other
	// entities (the ones they affect). Note that mechanisms aren't necessarily directly interactive with the player.
	class Mechanism : public Entity
	{
	protected:

		virtual ~Mechanism() = 0;
		virtual void Activate(Player& player) = 0;

		// All mechanisms have a local ID (although not all use them).
		void Initialize(const Json& j) override;

	public:

		// Local ID refers to the entity's index within objects of that type in the world. This value must be set in
		// order to create handles to the entity. That also means that mechanisms that aren't acted upon by OTHER
		// mechanisms can safely leave the ID at its default value.
		int localId = -1;
	};

	inline Mechanism::~Mechanism() = default;
}
