#pragma once
#include "Shape.h"
#include <functional>
#include <vector>
#include "SensorWorld.h"
#include "DoublyLinkedListNode.h"
#include "ISensitive.h"
#include "Enumerations.h"
#include "IDisposable.h"

namespace Sunspot
{
	class SensorWorld;
	class Sensor : public IDisposable
	{
	private:

		Shape* shape;
		ISensitive* parent;
		SensorTypes sensorType;

	public:

		// Using a static pointer felt like the simplest approach here.
		static SensorWorld* world;

		// Since the world organizes shapes based on type, the underlying shape must be known on creation.
		Sensor(SensorTypes type, Shape* shape, ISensitive* parent);

		// Using a destructor caused memory problems with the linked list.
		void Dispose() override;

		Shape* GetShape() const;
		ISensitive* GetParent() const;
		SensorTypes GetSensorType() const;
		DoublyLinkedListNode<Sensor*>* node = nullptr;

		bool enabled = true;

		// Using a list rather than a single value simplifies comparison to other sensors.
		std::vector<SensorTypes> canActivate;
		std::vector<Sensor*> contactList;
		std::vector<Sensor*> ignoreList;
	};
}
