#pragma once

namespace Sunspot
{
	class EntityHandle
	{
	public:

		EntityHandle() = default;
		explicit EntityHandle(void* data);

		void* data = nullptr;

		// Ideally, this class would use templates and overload the -> operator for easy data access, but that's not
		// possible given how handles are stored in the scene.
		template<class T>
		T* Get();
	};

	template<class T>
	T* EntityHandle::Get()
	{
		return static_cast<T*>(data);
	}
}
