#include "PrimitiveGroup.h"

namespace Sunspot
{
	PrimitiveGroup::PrimitiveGroup(const GLenum mode) : mode(mode)
	{
	}

	GLenum PrimitiveGroup::Mode() const
	{
		return mode;
	}

	void PrimitiveGroup::Clear()
	{
		vertices.clear();
		colors.clear();
		indices.clear();

		indexOffset = 0;
	}
}
