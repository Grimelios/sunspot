#include "Container2D.h"

namespace Sunspot
{
	glm::ivec2 Container2D::GetLocation() const
	{
		return bounds.GetLocation();
	}

	const Bounds& Container2D::GetBounds() const
	{
		return bounds;
	}

	void Container2D::SetWidth(const int width)
	{
		bounds.width = width;
	}

	void Container2D::SetHeight(const int height)
	{
		bounds.height = height;
	}

	void Container2D::SetLocation(const glm::ivec2& location)
	{
		if (centered)
		{
			bounds.SetCenter(location);
		}
		else
		{
			bounds.SetLocation(location);
		}
	}

	void Container2D::Update(const float dt)
	{
	}

	void Container2D::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
	}
}
