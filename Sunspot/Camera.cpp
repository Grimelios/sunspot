#include "Camera.h"
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Resolution.h"

namespace Sunspot
{
	const glm::mat4& Camera::View() const
	{
		return view;
	}

	void Camera::Update(const float dt)
	{
		glm::vec3 translation(glm::vec2(origin) - position, 0);
		translation.x /= Resolution::Width / 2;
		translation.y /= Resolution::Height / 2;

		const glm::mat4 matrix = translate(glm::mat4(1), translation);

		view = matrix;
	}
}
