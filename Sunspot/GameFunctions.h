#pragma once
#include <glm/vec2.hpp>
#include "Enumerations.h"
#include <string>
#include "Bounds.h"

namespace Sunspot::GameFunctions
{
	int Sign(float value);

	float ComputeAngle(const glm::vec2& value);
	float ComputeAngle(const glm::vec2& start, const glm::vec2& end);
	float LengthSquared(const glm::vec2& value);

	std::string ToString(const glm::vec2& value);
	std::string ToString(const Bounds& bounds);

	glm::vec2 ComputeDirection(float angle);
	glm::ivec2 ComputeOrigin(const int width, const int height, Alignments alignment);
}
