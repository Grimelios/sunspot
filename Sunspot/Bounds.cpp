#include "Bounds.h"

namespace Sunspot
{
	Bounds::Bounds() : Bounds(0, 0, 0, 0)
	{
	}

	Bounds::Bounds(const int width, const int height) : Bounds(0, 0, width, height)
	{
	}

	Bounds::Bounds(const int x, const int y, const int width, const int height) :
		x(x),
		y(y),
		width(width),
		height(height)
	{
	}

	Bounds::Bounds(const glm::ivec2& location, const int width, const int height) :
		Bounds(location.x, location.y, width, height)
	{
	}

	int Bounds::Left() const
	{
		return x;
	}

	int Bounds::Right() const
	{
		return x + width - 1;
	}

	int Bounds::Top() const
	{
		return y;
	}

	int Bounds::Bottom() const
	{
		return y + height - 1;
	}

	glm::ivec2 Bounds::GetLocation() const
	{
		return glm::ivec2(x, y);
	}

	glm::ivec2 Bounds::GetCenter() const
	{
		return glm::ivec2(x + width / 2, y + height / 2);
	}

	void Bounds::SetLocation(const glm::ivec2& location)
	{
		x = location.x;
		y = location.y;
	}

	void Bounds::SetCenter(const glm::ivec2& center)
	{
		x = center.x - width / 2;
		y = center.y - height / 2;
	}

	void Bounds::Left(const int left)
	{
		x = left;
	}

	void Bounds::Right(const int right)
	{
		x = right - width + 1;
	}

	void Bounds::Top(const int top)
	{
		y = top;
	}

	void Bounds::Bottom(const int bottom)
	{
		y = bottom - height + 1;
	}

	bool Bounds::Contains(const glm::vec2& position) const
	{
		return position.x >= x && position.x <= Right() && position.y >= y && position.y <= Bottom();
	}

	std::array<glm::vec2, 4> Bounds::GetCorners() const
	{
		// Adding one is required to render properly.
		const int right = Right() + 1;
		const int bottom = Bottom() + 1;

		return
		{
			glm::vec2(x, y),
			glm::vec2(right, y),
			glm::vec2(right, bottom),
			glm::vec2(x, bottom)
		};
	}

	Bounds Bounds::Scale(const int scalar) const
	{
		return Bounds(x * scalar, y * scalar, width * scalar, height * scalar);
	}
}
