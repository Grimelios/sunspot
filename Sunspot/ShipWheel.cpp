#include "ShipWheel.h"
#include "Scene.h"
#include "AxisBox.h"

namespace Sunspot
{
	const ShipWheelData ShipWheel::Data;

	ShipWheel::ShipWheel(const Json& j, Scene* scene) : sprite("ShipWheel.png")
	{
		std::vector<int> gateIds = j.at("Gates");
		gateHandles.reserve(gateIds.size());

		for (const int id : gateIds)
		{
			gateHandles.emplace_back();
			scene->GetHandle(EntityTypes::ShipGate, id, gateHandles.back());
		}

		components.push_back(&sprite);
		shapeOffset = glm::vec2(Data.SensorOffset(), 0);

		CreateSensor<AxisBox>(SensorTypes::Mechanism, Data.SensorWidth(), Data.SensorHeight());
		Entity::Initialize(j);
	}

	void ShipWheel::Activate(Player& player)
	{
		for (EntityHandle& gate : gateHandles)
		{
			gate.Get<ShipGate>()->Toggle();
		}
	}
}
