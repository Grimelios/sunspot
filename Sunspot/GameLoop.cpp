#include "GameLoop.h"

namespace Sunspot
{
	GameLoop::GameLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch& pb) :
		camera(camera),
		sb(sb),
		pb(pb)
	{
	}
}
