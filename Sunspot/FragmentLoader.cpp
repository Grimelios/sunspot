#include "FragmentLoader.h"
#include "JsonUtilities.h"
#include "Entity.h"

namespace Sunspot
{
	FragmentLoader::EntityList FragmentLoader::Load(const std::string& filename, Scene* scene)
	{
		std::vector<Json> jsonList = JsonUtilities::Load("Fragments/" + filename);
		
		EntityList entityList;

		for (const Json& j : jsonList)
		{
			entityList.push_back(entityFactory.Create(j, scene));
		}

		return entityList;
	}
}
