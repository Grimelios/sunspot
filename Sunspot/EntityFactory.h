#pragma once
#include <string>
#include <map>
#include <nlohmann/json.hpp>
#include "Enumerations.h"

namespace Sunspot
{
	class Scene;
	class Entity;
	class EntityFactory
	{
	private:

		using Json = nlohmann::json;

		std::map<std::string, EntityTypes> types;

	public:

		EntityFactory();

		std::unique_ptr<Entity> Create(const Json& j, Scene* scene);
	};
}
