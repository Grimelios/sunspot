#pragma once
#include "Container2D.h"
#include "SpriteText.h"
#include "ArrowButton.h"
#include "SelectableSet.h"
#include "EditorPanel.h"

namespace Sunspot
{
	class TilePanel : public EditorPanel
	{
	private:

		SpriteText tilesetText;
		std::unique_ptr<ArrowButton> leftButton;
		std::unique_ptr<ArrowButton> rightButton;
		std::vector<std::string> files;

		const Texture2D* tileset = nullptr;

		int tilesetIndex = 0;

		void Refresh();

	public:

		TilePanel();

		std::vector<ISelectable*> GetItems() const;

		void Location(const glm::ivec2& location) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
