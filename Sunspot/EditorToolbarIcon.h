#pragma once
#include "Sprite.h"
#include "Container2D.h"
#include "ISelectable.h"
#include "EditorToolbar.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	class EditorToolbar;
	class EditorToolbarIcon : public Container2D, public ISelectable
	{
	private:

		Sprite sprite;
		EditorToolbar* parent;

		std::string name;

		int index;

	public:

		EditorToolbarIcon(const Texture2D& texture, std::string name, int index, EditorToolbar* parent);

		// Publicly exposing this value allows icons to be toggled off when a new one is selected.
		bool selected = false;

		void OnHover() override;
		void OnUnhover() override;
		void OnSelect() override;
		bool Contains(const glm::vec2& mousePosition) override;
		void SetLocation(const glm::ivec2& location) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
