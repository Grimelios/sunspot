#include "EditorToolbarIcon.h"
#include "EditorConstants.h"
#include <glm/vec4.hpp>

namespace Sunspot
{
	EditorToolbarIcon::EditorToolbarIcon(const Texture2D& texture, std::string name, const int index,
		EditorToolbar* parent) :

		sprite(texture, Alignments::Left | Alignments::Top),
		parent(parent),
		name(std::move(name)),
		index(index)
	{
		const int iconSize = EditorConstants::IconSize;

		bounds.width = iconSize;
		bounds.height = iconSize;

		// This assumes that all toolbar icons will be in a straight line at the top of the image.
		sprite.Source(Bounds(iconSize * index, 0, iconSize, iconSize));
		sprite.color = glm::vec4(1, 1, 1, EditorConstants::InactiveOpacity);
	}

	void EditorToolbarIcon::OnHover()
	{
		sprite.Opacity(1);
	}

	void EditorToolbarIcon::OnUnhover()
	{
		if (!selected)
		{
			sprite.Opacity(EditorConstants::InactiveOpacity);
		}
	}

	void EditorToolbarIcon::OnSelect()
	{
		selected = true;
		parent->SetActive(index);
	}

	bool EditorToolbarIcon::Contains(const glm::vec2& mousePosition)
	{
		return bounds.Contains(mousePosition);
	}

	void EditorToolbarIcon::SetLocation(const glm::ivec2& location)
	{
		sprite.position = location;
		bounds.SetLocation(location);
	}

	void EditorToolbarIcon::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		sprite.Draw(sb, pb);
	}
}
