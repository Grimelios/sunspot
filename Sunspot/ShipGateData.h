#pragma once
#include "DataClass.h"

namespace Sunspot
{
	// Movement duration is taken from ShipWheelData since wheels conceptually control the gate directly.
	class ShipGateData : public DataClass
	{
	private:

		int distance;

	public:

		ShipGateData();

		int Distance() const;
	};
}
