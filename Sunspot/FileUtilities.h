#pragma once
#include <vector>
#include <string>

namespace Sunspot::FileUtilities
{
	std::string ReadAllText(const std::string& filename);
	std::vector<std::string> GetFiles(const std::string& directory);

	void SkipLine(std::ifstream& stream);
	void SkipLines(std::ifstream& stream, int count);
}
