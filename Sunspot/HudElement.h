#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	enum class HudElementTypes
	{
		Floor = 0,
		Health = 1,
		Zone = 2,
		Count = 3
	};

	class HudElement : public IDynamic, public IRenderable
	{
	public:

		// Making these variables public simplifies parent constructors (even though the values likely won't change
		// after construction).
		Alignments alignment = Alignments::Center;		
		glm::ivec2 offset = glm::ivec2(0);

		// Defaulting this value to true is pretty arbitrary. It'll probably be roughly 50/50 for elements that are
		// default visible vs. hidden.
		bool visible = true;

		virtual void SetLocation(const glm::ivec2& location) = 0;

		void Update(float dt) override = 0;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override = 0;
	};
}
