#include "Sector.h"
#include <glm/vec2.hpp>
#include <glm/gtc/constants.hpp>
#include "GameFunctions.h"

namespace Sunspot
{
	Sector::Sector() : Sector(0, 0, glm::vec2(0), 0)
	{
	}

	Sector::Sector(const int radius, const float spread) : Sector(radius, spread, glm::vec2(0), 0)
	{
	}

	Sector::Sector(const int radius, const float spread, const glm::vec2& center, const float rotation) :
		Shape(center, rotation, ShapeTypes::Sector),

		radius(radius),
		spread(spread)
	{
	}

	void Sector::ComputeCorners()
	{
		const float halfSpread = spread / 2;
		const float lower = rotation - halfSpread;
		const float upper = rotation + halfSpread;
		const float r = static_cast<float>(radius);

		lowerCorner = GameFunctions::ComputeDirection(lower) * r;
		upperCorner = GameFunctions::ComputeDirection(upper) * r;
	}

	bool Sector::Contains(const glm::vec2& point) const
	{
		const glm::vec2 v = point - position;

		if (GameFunctions::LengthSquared(v) > radius * radius)
		{
			return false;
		}

		float delta = GameFunctions::ComputeAngle(v) - rotation;

		if (delta > glm::half_pi<float>())
		{
			delta = glm::two_pi<float>() - delta;
		}

		return delta <= spread / 2;
	}
}
