#pragma once
#include "Component2D.h"
#include "SpriteFont.h"
#include "Enumerations.h"

namespace Sunspot
{
	class SpriteText : public Component2D
	{
	private:

		const SpriteFont& font;

		std::optional<std::string> value;

	public:

		SpriteText(const std::string& font, const std::optional<std::string>& value,
			Alignments alignment = Alignments::Left | Alignments::Top);
		SpriteText(const SpriteFont& font, std::optional<std::string> value,
			Alignments alignment = Alignments::Left | Alignments::Top);

		int Size() const;

		// Using getter and setter functions allows the origin to be re-computed when set.
		const std::optional<std::string>& Value() const;

		void Clear();
		void Value(std::optional<std::string> value);
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
