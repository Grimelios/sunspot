#pragma once

namespace Sunspot
{
	class IDisposable
	{
	public:

		virtual ~IDisposable() = default;
		virtual void Dispose() = 0;
	};
}
