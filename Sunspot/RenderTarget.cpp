#include "RenderTarget.h"

namespace Sunspot
{
	RenderTarget::RenderTarget(const int width, const int height) :
		width(width),
		height(height)
	{
		// See http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-14-render-to-texture/.
		glGenFramebuffers(1, &frameBuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

		// Texture
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		// Depth buffer
		glGenRenderbuffers(1, &depthBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureId, 0);

		GLenum drawBuffers[1] = { GL_COLOR_ATTACHMENT0 };

		glDrawBuffers(1, drawBuffers);

		// It's simpler to always reset the render target back to zero here.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	int RenderTarget::Width() const
	{
		return width;
	}

	int RenderTarget::Height() const
	{
		return height;
	}

	GLuint RenderTarget::TextureId() const
	{
		return textureId;
	}

	GLuint RenderTarget::FrameBuffer() const
	{
		return frameBuffer;
	}

	GLuint RenderTarget::DepthBuffer() const
	{
		return depthBuffer;
	}
}
