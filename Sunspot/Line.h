#pragma once
#include "Shape.h"

namespace Sunspot
{
	class Line : public Shape
	{
	public:

		Line(const glm::vec2& start, const glm::vec2& end);

		glm::vec2 start;
		glm::vec2 end;
	};
}
