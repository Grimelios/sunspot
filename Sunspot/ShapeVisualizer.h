#pragma once
#include "IRenderable.h"
#include "SensorWorld.h"
#include "AxisBox.h"
#include "Sector.h"
#include <glm/vec4.hpp>

namespace Sunspot
{
	class ShapeVisualizer
	{
	private:

		static const int MaxRadialSegmentLength = 20;

		const SensorWorld::SensorArray& sensors;

		glm::vec4 color = glm::vec4(1);

	public:

		explicit ShapeVisualizer(const SensorWorld& sensorWorld);

		void Draw(PrimitiveBatch& pb) const;
		void DrawShape(Shape& shape, PrimitiveBatch& pb) const;
		void DrawAxisBox(AxisBox& shape, PrimitiveBatch& pb) const;
		void DrawSector(Sector& sector, PrimitiveBatch& pb) const;
	};
}
