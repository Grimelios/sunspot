#include "Mechanism.h"

namespace Sunspot
{
	void Mechanism::Initialize(const Json& j)
	{
		localId = j.at("LocalId").get<int>();

		Entity::Initialize(j);
	}
}
