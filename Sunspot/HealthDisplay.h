#pragma once
#include "HudElement.h"
#include "Sprite.h"

namespace Sunspot
{
	class HealthDisplay : public HudElement
	{
	private:

		Sprite sprite;

	public:

		explicit HealthDisplay(const Texture2D& texture);

		void SetLocation(const glm::ivec2& location) override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
