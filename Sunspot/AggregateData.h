#pragma once
#include "InputData.h"
#include <array>
#include <vector>
#include "InputBind.h"

namespace Sunspot
{
	class AggregateData
	{
	private:

		static const int TypeCount = static_cast<int>(InputTypes::Count);

		using DataArray = std::array<InputData*, TypeCount>;

		DataArray dataArray;

	public:

		// The data array is initialized to all null, then filled in based on which input devices are active.
		AggregateData();

		template<class T>
		const T& Data(InputTypes type) const;

		void Data(InputTypes type, InputData& data);
		bool Query(const std::vector<InputBind>& binds, InputStates state) const;
	};

	template <class T>
	const T& AggregateData::Data(const InputTypes type) const
	{
		return *static_cast<T*>(dataArray[static_cast<int>(type)]);
	}
}
