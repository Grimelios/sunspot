#include "Parse.h"
#include <vector>
#include "StringUtilities.h"

namespace Sunspot
{
	glm::vec2 Parse::Vec2(const std::string& value)
	{
		// Vec2s are in the form "x,y" (note the lack of a space).
		const int index = StringUtilities::IndexOf(value, ',');

		const float x = std::stof(value.substr(0, index));
		const float y = std::stof(value.substr(index + 1, value.size() - index - 1));

		return glm::vec2(x, y);
	}
}
