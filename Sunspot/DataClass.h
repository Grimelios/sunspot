#pragma once
#include <string>
#include <nlohmann/json.hpp>

namespace Sunspot
{
	class DataClass
	{
	protected:

		using Json = nlohmann::json;

		Json Load(const std::string& filename) const;

	public:

		virtual ~DataClass() = default;
	};
}
