#pragma once
#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace Sunspot
{
	class SpriteGroup
	{
	public:

		std::vector<glm::vec3> vertices;
		std::vector<glm::vec2> texCoords;
		std::vector<glm::vec2> quadCoords;
		std::vector<glm::vec4> colors;
		std::vector<int> indices;

		int indexOffset = 0;

		void Clear();
	};
}
