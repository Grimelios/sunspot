#pragma once
#include "AxisBox.h"
#include "Sector.h"

namespace Sunspot
{
	class ShapeHelper
	{
	private:

		static bool CheckOverlap(AxisBox& a1, AxisBox& a2);
		static bool CheckOverlap(AxisBox& a, Sector& s);

	public:

		bool CheckOverlap(Shape& s1, Shape& s2) const;
	};
}
