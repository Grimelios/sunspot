#pragma once
#include "Container2D.h"
#include "SpriteText.h"

namespace Sunspot
{
	class EditorStatusBar : public Container2D
	{
	private:

		static const int XOffset = 7;

		SpriteText statusText;

	public:

		EditorStatusBar();

		void SetLocation(const glm::ivec2& location) override;
		void Append(const std::string& value);
		void Clear();
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
