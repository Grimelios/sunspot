#include "MouseData.h"

namespace Sunspot
{
	MouseData::MouseData(const glm::vec2& screenPosition, const glm::vec2& worldPosition,
		const glm::vec2& previousScreenPosition, const glm::vec2& previousWorldPosition, const ButtonArray& buttonArray) :

		screenPosition(screenPosition),
		worldPosition(worldPosition),
		previousScreenPosition(previousScreenPosition),
		previousWorldPosition(previousWorldPosition),
		buttonArray(buttonArray)
	{
	}

	glm::vec2 MouseData::ScreenPosition() const
	{
		return screenPosition;
	}

	glm::vec2 MouseData::WorldPosition() const
	{
		return worldPosition;
	}

	glm::vec2 MouseData::PreviousScreenPosition() const
	{
		return previousScreenPosition;
	}

	glm::vec2 MouseData::PreviousWorldPosition() const
	{
		return previousWorldPosition;
	}

	InputStates MouseData::State(const int button) const
	{
		return buttonArray[button];
	}

	bool MouseData::Query(const std::any& data, const InputStates state) const
	{
		const int button = std::any_cast<int>(data);

		InputStates s = buttonArray[button];

		return (buttonArray[button] & state) == state;
	}
}
