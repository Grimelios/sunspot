#include "HeadsUpDisplay.h"
#include "JsonUtilities.h"
#include "ZoneDisplay.h"
#include "HealthDisplay.h"
#include <vector>
#include "ContentCache.h"
#include "Messaging.h"
#include "Resolution.h"
#include "FloorDisplay.h"

namespace Sunspot
{
	HeadsUpDisplay::HeadsUpDisplay()
	{
		ElementMap map =
		{
			{ "Floor", HudElementTypes::Floor },
			{ "Health", HudElementTypes::Health },
			{ "Zone", HudElementTypes::Zone }
		};

		std::vector<Json> jsonList = JsonUtilities::Load("Hud.json");

		const Texture2D& texture = ContentCache::GetTexture("UI/Hud.png");

		for (const Json& j : jsonList)
		{
			const int x = j.at("OffsetX").get<int>();
			const int y = j.at("OffsetY").get<int>();

			const HudElementTypes type = map.at(j.at("Type").get<std::string>());

			ElementPointer element = CreateElement(type, texture);
			element->alignment = j.at("Alignment").get<Alignments>();
			element->offset = glm::ivec2(x, y);
			elements[static_cast<int>(type)] = std::move(element);
		}

		Messaging::Subscribe(MessageTypes::Resize, [this](const std::any& data, const float dt)
		{
			OnResize();
		});
	}

	HeadsUpDisplay::ElementPointer HeadsUpDisplay::CreateElement(const HudElementTypes type, const Texture2D& texture)
	{
		switch (type)
		{
			case HudElementTypes::Floor: return std::make_unique<FloorDisplay>();
			case HudElementTypes::Health: return std::make_unique<HealthDisplay>(texture);
			case HudElementTypes::Zone: return std::make_unique<ZoneDisplay>();
		}

		return nullptr;
	}

	void HeadsUpDisplay::OnResize()
	{
		const int width = Resolution::Target.width;
		const int height = Resolution::Target.height;

		for (ElementPointer& element : elements)
		{
			// This code is partially copied from GameFunctions::ComputeOrigin, but it seems different enough to
			// warrant copying rather than adding another helper function.
			const int alignmentValue = static_cast<int>(element->alignment);

			const bool left = (alignmentValue & static_cast<int>(Alignments::Left)) > 0;
			const bool right = (alignmentValue & static_cast<int>(Alignments::Right)) > 0;
			const bool top = (alignmentValue & static_cast<int>(Alignments::Top)) > 0;
			const bool bottom = (alignmentValue & static_cast<int>(Alignments::Bottom)) > 0;

			const int offsetX = element->offset.x;
			const int offsetY = element->offset.y;
			const int x = left ? offsetX : (right ? width - offsetX : width / 2 + offsetX);
			const int y = top ? offsetY : (bottom ? height - offsetY : height / 2 + offsetY);

			element->SetLocation(glm::ivec2(x, y));
		}
	}

	void HeadsUpDisplay::Update(const float dt)
	{
		for (ElementPointer& element : elements)
		{
			if (element->visible)
			{
				element->Update(dt);
			}
		}
	}

	void HeadsUpDisplay::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		for (ElementPointer& element : elements)
		{
			if (element->visible)
			{
				element->Draw(sb, pb);
			}
		}
	}
}
