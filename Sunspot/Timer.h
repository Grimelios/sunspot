#pragma once
#include "IDynamic.h"
#include "TimerCollection.h"
#include "DoublyLinkedListNode.h"
#include <functional>

namespace Sunspot
{
	class TimerCollection;
	class Timer : public IDynamic
	{
	public:

		using Tick = std::function<void(float)>;
		using RFunction = std::function<bool(float)>;
		using NFunction = std::function<void(float)>;

		Tick tick;
		RFunction rFunction;
		NFunction nFunction;

		bool repeating;
		bool completed = false;

	public:

		Timer(int duration, const NFunction& trigger, float elapsed = 0);
		Timer(int duration, Tick tick, NFunction trigger, float elapsed = 0);
		Timer(int duration, const RFunction& trigger, float elapsed = 0);
		Timer(int duration, Tick tick, RFunction trigger, float elapsed = 0);

		float elapsed = 0;
		float duration;

		bool paused = false;
		bool Completed() const;

		void Update(float dt) override;
	};
}
