#include "Sensor.h"

namespace Sunspot
{
	SensorWorld* Sensor::world = nullptr;

	Sensor::Sensor(const SensorTypes type, Shape* shape, ISensitive* parent) :
		shape(shape),
		parent(parent),
		sensorType(type)
	{
		world->Add(this);
	}

	void Sensor::Dispose()
	{
		world->Remove(this);
	}

	Shape* Sensor::GetShape() const
	{
		return shape;
	}

	ISensitive* Sensor::GetParent() const
	{
		return parent;
	}

	SensorTypes Sensor::GetSensorType() const
	{
		return sensorType;
	}
}
