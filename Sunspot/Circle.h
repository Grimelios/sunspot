#pragma once
#include "Shape.h"

namespace Sunspot
{
	class Circle : public Shape
	{
	public:

		Circle();
		explicit Circle(float radius);
		explicit Circle(float radius, const glm::vec2& center);

		float radius;
	};
}
