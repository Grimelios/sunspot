#include "Scene.h"
#include "Entity.h"
#include "MapUtilities.h"

namespace Sunspot
{
	void Scene::LoadFragment(const std::string& filename)
	{
		for (EntityPointer& e : fragmentLoader.Load(filename, this))
		{
			AddInternal(e);
		}

		handleList.clear();
	}

	void Scene::RegisterHandle(const EntityTypes type, void* entity, const int id)
	{
		EntityHandle*& handle = VerifyHandles(type, id);

		// This indicates that the handle has not yet been created (i.e. an external class hasn't yet requested the
		// given handle).
		if (handle == nullptr)
		{
			handleList.emplace_back(entity);
			handle = &handleList.back();
		}
		else
		{
			handle->data = entity;
		}
	}

	void Scene::GetHandle(const EntityTypes type, const int id, EntityHandle& handle)
	{
		// H stands for "handle", but there's already a variable named handle (the one passed in).
		EntityHandle*& h = VerifyHandles(type, id);

		// Again, this indicates that the requested handle has not yet been registered.
		if (h == nullptr)
		{
			h = &handle;
		}
		else
		{
			handle.data = h->data;
		}
	}

	EntityHandle*& Scene::VerifyHandles(const EntityTypes type, const int id)
	{
		const auto i = handleMap.find(type);

		auto& list = i != handleMap.end()
			? i->second
			: MapUtilities::Add(handleMap, type, std::vector<EntityHandle*>());

		const int size = static_cast<int>(list.size());

		if (size <= id)
		{
			list.insert(list.end(), id - size + 1, nullptr);
		}

		return list[id];
	}

	Entity* Scene::AddInternal(EntityPointer& e)
	{
		EntityNode* node = entities.Add(e);
		Entity* entity = node->data.get();
		entity->scene = this;
		entity->node = node;

		return entity;
	}

	void Scene::Remove(Entity* entity)
	{
		EntityNode* node = entity->node;
		node->data.reset();
		entities.Remove(node);
	}

	void Scene::Update(const float dt)
	{
		EntityNode* node = entities.Head();

		while (node != nullptr)
		{
			node->data->Update(dt);
			node = node->next;
		}
	}

	void Scene::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		EntityNode* node = entities.Head();

		while (node != nullptr)
		{
			node->data->Draw(sb, pb);
			node = node->next;
		}
	}
}
