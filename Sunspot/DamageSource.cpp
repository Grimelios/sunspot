#include "DamageSource.h"

namespace Sunspot
{
	DamageSource::DamageSource(Entity* parent) :
		parent(parent)
	{
	}

	int DamageSource::Damage() const
	{
		return damage;
	}

	int DamageSource::Knockback() const
	{
		return knockback;
	}

	Entity* DamageSource::Parent() const
	{
		return parent;
	}
}
