#include "InputBind.h"
#include <utility>

namespace Sunspot
{
	InputBind::InputBind(const InputTypes type, std::any data) :
		type(type),
		data(std::move(data))
	{
	}
}
