#include "TilePanel.h"
#include <vector>
#include "Paths.h"
#include "FileUtilities.h"
#include "ContentCache.h"
#include "EditorConstants.h"

namespace Sunspot
{
	TilePanel::TilePanel() :
		tilesetText("Editor", std::nullopt, Alignments::Center)
	{	
		files = FileUtilities::GetFiles(Paths::Textures + "Tilesets/");

		const int iconSize = EditorConstants::IconSize;

		const Texture2D& spritesheet(ContentCache::GetTexture("UI/Editor.png"));
		const Bounds source(0, iconSize, iconSize, iconSize);

		leftButton = std::make_unique<ArrowButton>(spritesheet, source);
		leftButton->clickFunction = [this]()
		{
			tilesetIndex = tilesetIndex == 0 ? files.size() - 1 : --tilesetIndex;

			Refresh();
		};

		rightButton = std::make_unique<ArrowButton>(spritesheet, source, true);
		rightButton->clickFunction = [this]()
		{
			tilesetIndex = tilesetIndex == files.size() - 1 ? 0 : ++tilesetIndex;

			Refresh();
		};

		Refresh();
	}

	void TilePanel::Refresh()
	{
		const std::string& filename = files[tilesetIndex];

		tilesetText.Value(filename);
		tileset = &ContentCache::GetTexture("Tilesets/" + filename);
	}

	std::vector<ISelectable*> TilePanel::GetItems() const
	{
		std::vector<ISelectable*> items;
		items.reserve(2);
		items.push_back(leftButton.get());
		items.push_back(rightButton.get());

		return items;
	}

	void TilePanel::Location(const glm::ivec2& location)
	{
		const int padding = EditorConstants::Padding;
		const int middleY = location.y + padding + EditorConstants::HalfIconSize;
		const int buttonOffset = padding + EditorConstants::HalfIconSize;

		tilesetText.position.x = static_cast<float>(location.x + bounds->width / 2);
		tilesetText.position.y = static_cast<float>(middleY);
		leftButton->SetLocation(glm::ivec2(location.x + buttonOffset, middleY));
		rightButton->SetLocation(glm::ivec2(bounds->Right() - buttonOffset, middleY));
	}

	void TilePanel::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		tilesetText.Draw(sb, pb);
		leftButton->Draw(sb, pb);
		rightButton->Draw(sb, pb);

		const glm::ivec2 tilesetLocation = bounds->GetLocation() + glm::ivec2(2, 38);

		sb.Draw(*tileset, tilesetLocation);
	}
}
