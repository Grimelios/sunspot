#pragma once
#include "InputData.h"
#include <vector>
#include <array>
#include "KeyPress.h"
#include <glad.h>
#include <glfw3.h>

namespace Sunspot
{
	class KeyboardData : public InputData
	{
	private:

		using KeyArray = std::array<InputStates, GLFW_KEY_LAST>;

		// In practice, full key presses (including key modifiers) are only required for keys pressed this frame.
		std::vector<int> keysHeld;
		std::vector<KeyPress> keysPressedThisFrame;
		std::vector<int> keysReleasedThisFrame;

		const KeyArray& keyArray;

	public:

		KeyboardData(std::vector<int> keysHeld, std::vector<KeyPress> keysPressedThisFrame,
					std::vector<int> keysReleasedThisFrame, const KeyArray& keyArray);

		const std::vector<int>& KeysHeld() const;
		const std::vector<KeyPress>& KeysPressedThisFrame() const;
		const std::vector<int>& KeysReleasedThisFrame() const;

		bool Query(const std::any& data, InputStates state) const override;
	};
}
