#include "ShipWheelData.h"

namespace Sunspot
{
	ShipWheelData::ShipWheelData()
	{
		Json j = Load("ShipWheelData.json");

		rotation = j.at("Rotation").get<float>();
		time = j.at("Time").get<int>();
		sensorWidth = j.at("SensorWidth").get<int>();
		sensorHeight = j.at("SensorHeight").get<int>();
		sensorOffset = j.at("SensorOffset").get<int>();
	}

	float ShipWheelData::Rotation() const
	{
		return rotation;
	}

	int ShipWheelData::Time() const
	{
		return time;
	}

	int ShipWheelData::SensorWidth() const
	{
		return sensorWidth;
	}

	int ShipWheelData::SensorHeight() const
	{
		return sensorHeight;
	}

	int ShipWheelData::SensorOffset() const
	{
		return sensorOffset;
	}
}
