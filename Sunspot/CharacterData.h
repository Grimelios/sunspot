#pragma once
#include <glm/vec2.hpp>
#include <array>

namespace Sunspot
{
	class CharacterData
	{
	private:

		int width;
		int height;
		int advance;

		glm::ivec2 offset;
		std::array<glm::vec2, 4> texCoords;

	public:

		CharacterData(int x, int y, int width, int height, int advance, int tWidth, int tHeight, const glm::ivec2& offset);

		int Width() const;
		int Height() const;
		int Advance() const;

		const glm::ivec2& Offset() const;
		const std::array<glm::vec2, 4>& TexCoords() const;
	};
}
