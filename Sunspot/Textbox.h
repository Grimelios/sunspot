#pragma once
#include "KeyboardData.h"
#include <string>
#include <array>
#include <optional>
#include "TextboxContainer.h"

namespace Sunspot
{
	class TextboxContainer;
	class Textbox
	{
	private:

		using NumericSpecials = std::array<char, 10>;

		static NumericSpecials numericSpecials;
		
		static std::optional<char> GetCharacter(int key, bool shift, bool capsLock);

		std::string value;

		int cursor = 0;

	public:

		// The container is owned by external classes rather than the textbox itself.
		TextboxContainer* container;

		void HandleKeyboard(const KeyboardData& data);
	};
}
