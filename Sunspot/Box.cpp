#include "Box.h"

namespace Sunspot
{
	Box::Box() : Box(0, 0, glm::vec2(0), 0)
	{
	}

	Box::Box(const int width, const int height) : Box(width, height, glm::vec2(0), 0)
	{
	}

	Box::Box(const int width, const int height, const glm::vec2& center, const float rotation) :
		Shape(center, rotation, ShapeTypes::Box),

		width(width),
		height(height)
	{
	}
}
