#pragma once
#include "Texture2D.h"
#include "SpriteFont.h"
#include <glm/vec2.hpp>
#include "Shader.h"
#include <map>
#include <array>
#include "SpriteGroup.h"
#include "PrimitiveBatch.h"
#include "BatchBase.h"
#include <optional>
#include "RenderTarget.h"
#include "Enumerations.h"

namespace Sunspot
{
	class PrimitiveBatch;
	class SpriteBatch : public BatchBase
	{
	private:

		using InnerMap = std::map<GLuint, SpriteGroup>;
		using SpriteMap = std::map<const Shader*, InnerMap>;

		const Shader spriteShader;

		GLint flipLocation = 0;
		GLint mvpLocation = 0;

		GLuint vao = 0;
		GLuint program = 0;
		GLuint indexBuffer = 0;
		GLuint positionBuffer = 0;
		GLuint texCoordBuffer = 0;
		GLuint quadCoordBuffer = 0;
		GLuint colorBuffer = 0;

		std::array<glm::vec2, 4> quadTexCoords;
		std::array<int, 6> quadIndices;

		SpriteMap spriteMap;
		PrimitiveBatch* pb = nullptr;
		SpriteGroup* activeGroup = nullptr;
		InnerMap* activeMap = nullptr;

		// Storing an ID rather than a Texture2D allows render targets to be used as well.
		GLuint activeTextureId = 0;

		// Position and origin are passed in by value because they're changed in the function.
		void Draw(GLuint textureId, int width, int height, const std::optional<Bounds>& source, glm::vec2 position,
			glm::ivec2 origin, const glm::vec4& color, float rotation, const glm::vec2& scale, SpriteModifiers mods);

		void Flush(BatchCoords coords) override;
		void Flush(SpriteGroup& group) const;
		void VerifyTexture(GLuint textureId);

	public:

		explicit SpriteBatch(const Camera& camera);

		void SetOther(PrimitiveBatch* pb);
		void SetTarget(const RenderTarget* target) const;
		void Apply(const Shader& shader);

		void Draw(const Texture2D& texture, const glm::vec2& position);
		void Draw(const Texture2D& texture, const std::optional<Bounds>& source, const glm::vec2& position);
		void Draw(const Texture2D& texture, const std::optional<Bounds>& source, const glm::vec2& position,
			const glm::ivec2& origin, const glm::vec4& color, float rotation, const glm::vec2& scale, SpriteModifiers mods);

		void Draw(const RenderTarget& target, const glm::vec2& position);

		void DrawString(const SpriteFont& font, const std::string& value, const glm::vec2& position,
			const glm::ivec2& origin, float rotation, const glm::vec4& color, const glm::vec2& scale);
	};
}
