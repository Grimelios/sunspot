#include "Shader.h"
#include "Paths.h"
#include <vector>
#include <iostream>
#include "FileUtilities.h"

namespace Sunspot
{
	Shader::Shader(const std::string& name) : Shader(name + ".vert", name + ".frag")
	{
	}

	Shader::Shader(const std::string& vShader, const std::string& fShader)
	{
		LoadShader(vShader, GL_VERTEX_SHADER, this->vShader);
		LoadShader(fShader, GL_FRAGMENT_SHADER, this->fShader);
		CreateProgram();
		GetUniforms();
	}

	void Shader::LoadShader(const std::string& filename, const GLenum type, GLuint& shaderId)
	{
		shaderId = glCreateShader(type);

		// See https://stackoverflow.com/questions/22100408/what-is-the-meaning-of-the-parameters-to-glshadersource.
		std::string source = FileUtilities::ReadAllText(Paths::Shaders + filename);

		GLchar const* file = source.c_str();
		GLint length = static_cast<GLint>(source.size());

		glShaderSource(shaderId, 1, &file, &length);
		glCompileShader(shaderId);

		GLint status;

		glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);

		if (status == GL_FALSE)
		{
			GLint logSize = 0;

			glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logSize);

			std::vector<GLchar> message = std::vector<GLchar>(logSize);

			glGetShaderInfoLog(shaderId, logSize, nullptr, &message[0]);
			glDeleteShader(shaderId);

			std::cout << std::string(message.begin(), message.end());
		}
	}

	void Shader::CreateProgram()
	{
		program = glCreateProgram();

		glAttachShader(program, vShader);
		glAttachShader(program, fShader);
		glLinkProgram(program);

		int status;

		glGetProgramiv(program, GL_LINK_STATUS, &status);

		if (status == GL_FALSE)
		{
			GLint logSize = 0;

			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logSize);

			std::vector<GLchar> message = std::vector<GLchar>(logSize);

			glGetProgramInfoLog(program, logSize, nullptr, &message[0]);
			glDeleteProgram(program);
			glDeleteShader(vShader);
			glDeleteShader(fShader);

			std::cout << std::string(message.begin(), message.end());
		}

		glDeleteShader(vShader);
		glDeleteShader(fShader);
	}

	void Shader::GetUniforms()
	{
		const int bufferSize = 32;

		GLint uniformCount;
		GLsizei length;
		GLint size;
		GLenum type;

		std::vector<GLchar> name = std::vector<GLchar>(bufferSize);

		glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &uniformCount);

		for (GLint i = 0; i < uniformCount; i++)
		{
			glGetActiveUniform(program, i, bufferSize, &length, &size, &type, &name[0]);

			std::string nameString;

			for (int j = 0; j < length; j++)
			{
				nameString.push_back(name[j]);
			}

			GLint location = glGetUniformLocation(program, &nameString[0]);

			uniformMap.insert(UniformMap::value_type(std::move(nameString), location));
		}
	}

	GLuint Shader::Program() const
	{
		return program;
	}

	GLint Shader::Uniforms(const std::string& name)
	{
		return uniformMap.at(name);
	}
}
