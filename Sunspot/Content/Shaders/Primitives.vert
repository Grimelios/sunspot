#version 330 core

// 3D positions are used for Z-sorting.
layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec4 vColor;

out vec4 fColor;

uniform mat4 mvp;
uniform int flip;

void main()
{
	vec4 position = mvp * vec4(vPosition, 1);
	position.y *= flip;

	gl_Position = position;
	fColor = vColor;
}
