#include "EditorTextboxContainer.h"

namespace Sunspot
{
	EditorTextboxContainer::EditorTextboxContainer() :
		spriteText("Editor", std::nullopt)
	{
	}

	void EditorTextboxContainer::Clear()
	{
		spriteText.Value(std::nullopt);
	}

	void EditorTextboxContainer::Cursor(const int cursor)
	{
	}

	void EditorTextboxContainer::Location(const glm::ivec2& location)
	{
		spriteText.position = location;
	}

	void EditorTextboxContainer::Value(const std::string& value)
	{
		spriteText.Value(value);
	}

	void EditorTextboxContainer::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		spriteText.Draw(sb, pb);
	}
}
