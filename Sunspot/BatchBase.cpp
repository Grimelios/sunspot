#include "BatchBase.h"
#include "Messaging.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glad.h>

namespace Sunspot
{
	const float BatchBase::ZStart = 0.999f;
	const float BatchBase::ZIncrement = 0.0001f;

	BatchBase::BatchBase(const Camera& camera) : camera(camera)
	{
		Messaging::Subscribe(MessageTypes::Resize, [this](const std::any& data, const float dt)
		{
			const glm::ivec2& dimensions = std::any_cast<glm::ivec2>(data);
			const glm::ivec2& halfDimensions = std::any_cast<glm::ivec2>(data) / 2;

			projection = glm::ortho(0.0f, static_cast<float>(dimensions.x), 0.0f, static_cast<float>(dimensions.y));
			screenMatrix = scale(glm::mat4(1), glm::vec3(1.0f / halfDimensions.x, 1.0f / halfDimensions.y, 1));
			screenMatrix = translate(screenMatrix, glm::vec3(-halfDimensions, 0));
		});
	}

	void BatchBase::Begin(const BatchCoords coords)
	{
		if (drawing)
		{
			throw std::runtime_error("The batch must be ended before restarting.");
		}

		this->coords = coords;

		drawing = true;
	}

	void BatchBase::End()
	{
		if (!drawing)
		{
			throw std::runtime_error("The batch must be started being ending.");
		}

		Flush(coords);

		drawing = false;
	}

	void BatchBase::ResetZ()
	{
		zValue = ZStart;
	}
}
