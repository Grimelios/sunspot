#pragma once
#include "Entity.h"
#include "TilemapData.h"
#include "TilemapRender.h"

namespace Sunspot
{
	class Tilemap : public Entity
	{
	private:

		// Using a separate rendering class allows non-tilemap classes to render tilemaps with less overhead. That
		// makes this entity mostly a wrapper around that renderer (with Entity features as well).
		TilemapRender render;

	public:

		explicit Tilemap(const std::string& filename);
		explicit Tilemap(const TilemapData& data);

		void SetPosition(const glm::vec2& position) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
