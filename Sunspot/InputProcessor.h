#pragma once
#include "IDynamic.h"
#include "KeyPress.h"
#include <vector>
#include <glad.h>
#include <glfw3.h>
#include <array>
#include "KeyboardData.h"
#include "MouseData.h"
#include "Camera.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	class InputProcessor : public IDynamic
	{
	private:

		const Camera& camera;

		std::array<InputStates, GLFW_KEY_LAST> keyArray;
		std::array<InputStates, GLFW_MOUSE_BUTTON_LAST> buttonArray;
		std::vector<KeyPress> keyPresses;

		glm::vec2 mousePosition;
		glm::vec2 previousScreenPosition = glm::vec2(0);
		glm::vec2 previousWorldPosition = glm::vec2(0);

		KeyboardData CreateKeyboardData();
		MouseData CreateMouseData();

	public:

		explicit InputProcessor(const Camera& camera);

		void OnKeyPress(int key, int mods);
		void OnKeyRelease(int key);
		void OnMouseButtonPress(int button);
		void OnMouseButtonRelease(int button);
		void OnMouseMove(float x, float y);
		void Update(float dt) override;
	};
}
