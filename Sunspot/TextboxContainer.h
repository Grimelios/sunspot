#pragma once
#include "IRenderable.h"

namespace Sunspot
{
	class Textbox;
	class TextboxContainer : public IRenderable
	{
	public:

		virtual ~TextboxContainer() = 0;

		const Textbox* textbox;

		virtual void Clear() = 0;
		virtual void Cursor(int cursor) = 0;
		virtual void Location(const glm::ivec2& location) = 0;
		virtual void Value(const std::string& value) = 0;
	};

	inline TextboxContainer::~TextboxContainer() = default;
}
