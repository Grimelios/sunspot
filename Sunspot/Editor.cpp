#include "Editor.h"
#include "Resolution.h"
#include "Messaging.h"
#include "TileTool.h"
#include "CameraTool.h"
#include "GameFunctions.h"

namespace Sunspot
{
	Editor::Editor(Camera& camera) :
		toolArray(
		{
			std::make_unique<CameraTool>(camera),
			std::make_unique<TileTool>()
		}),

		camera(camera),
		toolbar(std::make_unique<EditorToolbar>(selectableSet, this))
	{
		const int innerWidth = 300;
		const int innerBottom = Resolution::Height - borderSize;
		const int innerRight = Resolution::Width - borderSize;
		const int toolbarHeight = 36;

		const float backgroundGreyscale = 30.0f / 255;
		const float borderGreyscale = 15.0f / 255;

		toolbar->SetLocation(glm::ivec2(borderSize, borderSize));
		toolbar->SetWidth(innerWidth);
		toolbar->SetHeight(toolbarHeight);

		toolboxBounds = Bounds(borderSize, toolbarHeight + borderSize * 2, innerWidth, 0);
		toolboxBounds.height = innerBottom - toolboxBounds.y;

		const int statusBarX = toolboxBounds.Right() + borderSize + 1;
		const int statusBarHeight = 32;

		statusBar.SetWidth(innerRight - statusBarX);
		statusBar.SetHeight(statusBarHeight);
		statusBar.SetLocation(glm::ivec2(statusBarX, innerBottom - statusBarHeight));

		const Bounds& statusBounds = statusBar.GetBounds();

		gameplayBounds = Bounds(statusBarX, borderSize, 0, 0);
		gameplayBounds.width = statusBounds.width;
		gameplayBounds.height = statusBounds.Top() - borderSize * 2;

		fullBounds = Bounds(Resolution::Width, Resolution::Height);
		backgroundColor = glm::vec4(backgroundGreyscale);
		backgroundColor.a = 1;
		borderColor = glm::vec4(borderGreyscale);
		borderColor.a = 1;

		// This assumes that the camera tool will be the first in the list.
		cameraTool = static_cast<CameraTool*>(toolArray[0].get());
		activeTool = toolArray[1].get();

		for (std::unique_ptr<EditorTool>& tool : toolArray)
		{
			tool->statusBar = &statusBar;
		}
	}

	Bounds Editor::GetGameplayBounds() const
	{
		return gameplayBounds;
	}

	EditorTool* Editor::ActiveTool() const
	{
		return activeTool;
	}

	void Editor::Activate()
	{
		messagingKeyboardIndex = Messaging::Subscribe(MessageTypes::Keyboard, [this](const std::any& data, const float dt)
		{
			HandleKeyboard(std::any_cast<KeyboardData>(data));
		});

		messagingMouseIndex = Messaging::Subscribe(MessageTypes::Mouse, [this](const std::any& data, const float dt)
		{
			HandleMouse(std::any_cast<MouseData>(data));
		});
	}

	void Editor::Deactivate()
	{
		Messaging::Unsubscribe(MessageTypes::Keyboard, messagingKeyboardIndex);
		Messaging::Unsubscribe(MessageTypes::Mouse, messagingMouseIndex);

		messagingKeyboardIndex = -1;
		messagingMouseIndex = -1;
	}

	void Editor::Reload(Scene& scene)
	{
		for (std::unique_ptr<EditorTool>& tool : toolArray)
		{
			tool->Reload(scene);
		}
	}

	void Editor::SelectTool(const int index)
	{
		activeTool = toolArray[index].get();
		activePanel = activeTool->Panel();

		if (activePanel != nullptr)
		{
			activePanel->bounds = &toolboxBounds;
			activePanel->Location(toolboxBounds.GetLocation());
		}
	}

	bool Editor::Active() const
	{
		return active;
	}

	bool Editor::MouseWithinGameplayBounds() const
	{
		return mouseWithinGameplayBounds;
	}

	void Editor::HandleKeyboard(const KeyboardData& data)
	{
	}

	void Editor::HandleMouse(const MouseData& data)
	{
		const glm::vec2& mousePosition = data.WorldPosition();

		mouseWithinGameplayBounds = gameplayBounds.Contains(data.ScreenPosition());

		if (mouseWithinGameplayBounds)
		{
			// Manually un-hovering here allows the whole set to be ignored while the mouse is within gameplay bounds.
			if (selectableSet.hoveredItem != nullptr)
			{
				selectableSet.hoveredItem->OnUnhover();
				selectableSet.hoveredItem = nullptr;
			}

			const std::string mouseString = "Mouse: " + GameFunctions::ToString(mousePosition);
			const std::string cameraString = "Camera: " + GameFunctions::ToString(camera.position);

			statusBar.Append(mouseString);
			statusBar.Append(cameraString);
			cameraTool->HandleMouse(data);

			// It's possible the active tool will never be null, but it's safer to check anyway (maybe it'll be null when
			// the editor first launches).
			if (activeTool != nullptr)
			{
				activeTool->HandleMouse(data);
			}
		}
		else
		{
			selectableSet.HandleMouse(std::any_cast<MouseData>(data));
		}
	}

	void Editor::Update(const float dt)
	{
	}

	void Editor::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		pb.Begin(BatchCoords::Screen);
		pb.FillBounds(fullBounds, borderColor);
		pb.FillBounds(toolbar->GetBounds(), backgroundColor);
		pb.FillBounds(toolboxBounds, backgroundColor);
		pb.FillBounds(statusBar.GetBounds(), backgroundColor);
		pb.End();

		sb.Begin(BatchCoords::Screen);

		toolbar->Draw(sb, pb);
		statusBar.Draw(sb, pb);
		statusBar.Clear();

		if (activePanel != nullptr)
		{
			activePanel->Draw(sb, pb);
		}

		sb.End();
	}
}
