#include "AggregateData.h"

namespace Sunspot
{
	AggregateData::AggregateData()
	{
		for (int i = 0; i < TypeCount; i++)
		{
			dataArray[i] = nullptr;
		}
	}

	void AggregateData::Data(InputTypes type, InputData& data)
	{
		dataArray[static_cast<int>(type)] = &data;
	}

	bool AggregateData::Query(const std::vector<InputBind>& binds, const InputStates state) const
	{
		for (const InputBind& bind : binds)
		{
			if (dataArray[static_cast<int>(bind.type)]->Query(bind.data, state))
			{
				return true;
			}
		}

		return false;
	}
}
