#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include "KeyboardData.h"
#include "MouseData.h"

namespace Sunspot
{
	class EditorPanel : public IDynamic, public IRenderable
	{
	public:

		Bounds* bounds = nullptr;

		// Note that since bounds use a pointer, calling this function does NOT position the bounds.
		virtual void Location(const glm::ivec2& location) = 0;
		virtual void HandleKeyboard(const KeyboardData& data);
		virtual void HandleMouse(const MouseData& data);

		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override = 0;
	};
}
