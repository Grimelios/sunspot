#include "Button.h"
#include "ContentCache.h"

namespace Sunspot
{
	Button::Button(const std::string& texture, const Bounds& source) :
		Button(ContentCache::GetTexture(texture), source)
	{
	}

	Button::Button(const Texture2D& texture, const Bounds& source) :
		icon(texture, source)
	{
	}

	void Button::OnSelect()
	{
		clickFunction();
	}

	bool Button::Contains(const glm::vec2& mousePosition)
	{
		return bounds.Contains(mousePosition);
	}

	void Button::SetLocation(const glm::ivec2& location)
	{
		icon.position = location;

		// This assumes that all buttons are centered.
		bounds.SetCenter(location);
	}

	void Button::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		icon.Draw(sb, pb);
	}
}
