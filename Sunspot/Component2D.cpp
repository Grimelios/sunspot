#include "Component2D.h"

namespace Sunspot
{
	Component2D::Component2D(const Alignments alignment) : alignment(alignment)
	{
	}

	void Component2D::Opacity(const float opacity)
	{
		color.a = opacity;
	}
}
