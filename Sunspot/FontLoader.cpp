#include "FontLoader.h"
#include "ContentCache.h"
#include <vector>
#include "StringUtilities.h"
#include <fstream>
#include "FileUtilities.h"

namespace Sunspot
{
	SpriteFont FontLoader::Load(const std::string& name, const bool useKerning) const
	{
		const Texture2D& texture = ContentCache::GetTexture(name + "_0.png", Paths::Fonts);

		const int tWidth = texture.Width();
		const int tHeight = texture.Height();

		std::ifstream stream(Paths::Fonts + name + ".fnt");
		std::string line;
		std::getline(stream, line);

		// The third token on the first line is "size=[value]", but spaces in the font name must be accounted for.
		const int index1 = StringUtilities::IndexOf(line, "size") + 5;
		const int index2 = StringUtilities::IndexOf(line, ' ', index1);
		const int size = std::stoi(line.substr(index1, index2 - index1));

		FileUtilities::SkipLines(stream, 2);

		std::getline(stream, line);

		// The fourth line looks like "chars count=XX".
		const int charCount = std::stoi(line.substr(12));

		SpriteFont::CharacterArray dataArray;

		for (int i = 0; i < charCount; i++)
		{
			std::getline(stream, line);
			std::vector<std::string> tokens = StringUtilities::Split(line, ' ', true);

			const int id = ParseValue(tokens[1]);
			const int x = ParseValue(tokens[2]);
			const int y = ParseValue(tokens[3]);
			const int width = ParseValue(tokens[4]);
			const int height = ParseValue(tokens[5]);
			const int offsetX = ParseValue(tokens[6]);
			const int offsetY = ParseValue(tokens[7]);
			const int advance = ParseValue(tokens[8]);

			dataArray[id] = CharacterData(x, y, width, height, advance, tWidth, tHeight, glm::ivec2(offsetX, offsetY));
		}

		if (useKerning)
		{
		}

		return SpriteFont(texture, size, dataArray);
	}

	int FontLoader::ParseValue(const std::string& s) const
	{
		const int index = StringUtilities::IndexOf(s, '=') + 1;

		return std::stoi(s.substr(index));
	}

	void FontLoader::ParseKerning()
	{
	}
}
