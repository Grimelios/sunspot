#include "ShipGate.h"
#include "Ease.h"
#include "Scene.h"

namespace Sunspot
{
	const ShipGateData ShipGate::Data;

	ShipGate::ShipGate(const Json& j, Scene* scene) :
		render("ShipGate.json")
	{
		open = j.at("Open").get<bool>();
		renderBounds = render.GetBounds();

		// The local ID must be parsed before registering a handle with the scene.
		Mechanism::Initialize(j);

		scene->RegisterHandle(EntityTypes::ShipGate, this, localId);
	}

	void ShipGate::Activate(Player& player)
	{
	}

	void ShipGate::Toggle()
	{
		glm::vec2 start;
		glm::vec2 end;

		if (open)
		{
			start = openPosition;
			end = closedPosition;
		}
		else
		{
			start = closedPosition;
			end = openPosition;
		}

		timerCollection.timers.Add(ShipWheel::Data.Time(),
		
		[this, start, end](const float progress)
		{
			SetPositionInternal(Ease::Linear(start, end, progress));
		},

		[this, end](const float time)
		{
			SetPositionInternal(end);
		});

		open = !open;
	}

	void ShipGate::SetPosition(const glm::vec2& position)
	{
		closedPosition = position;
		openPosition = position - glm::vec2(0, Data.Distance());

		// This assumes that the gate as a whole won't be repositioned while moving.
		SetPositionInternal(open ? openPosition : closedPosition);
	}

	void ShipGate::SetPositionInternal(const glm::vec2& position)
	{
		render.SetPosition(position);

		Entity::SetPosition(position);
	}

	void ShipGate::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		render.Draw(sb, pb);
	}
}
