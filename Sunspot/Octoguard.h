#pragma once
#include "Enemy.h"

namespace Sunspot
{
	class Octoguard : public Enemy
	{
	private:

		Sprite sprite;

	protected:

		void AI(float dt) override;

	public:

		explicit Octoguard(const Json& j);
	};
}
