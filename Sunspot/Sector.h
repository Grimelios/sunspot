#pragma once
#include "Shape.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	class Sector : public Shape
	{
	public:

		Sector();
		Sector(int radius, float spread);
		Sector(int radius, float spread, const glm::vec2& center, float rotation);

		int radius;

		float spread;

		glm::vec2 lowerCorner = glm::vec2(0);
		glm::vec2 upperCorner = glm::vec2(0);

		void ComputeCorners();

		bool Contains(const glm::vec2& point) const;
	};
}
