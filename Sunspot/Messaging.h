#pragma once
#include <vector>
#include <optional>
#include <functional>
#include <array>
#include <any>

namespace Sunspot
{
	enum class MessageTypes
	{
		Keyboard = 0,
		Mouse = 1,
		Input = 2,
		Exit = 3,
		Resize = 4,
		Count = 5
	};

	class Messaging
	{
	private:

		static const int TypeCount = static_cast<int>(MessageTypes::Count);

		// Functions are stored as optional due to the way actions are stored in an array. When a receiver unsubscribes, its
		// corresponding index can be set to nullopt while leaving later functions intact.
		using ReceiverFunction = std::function<void(std::any, float)>;
		using ReceiverVector = std::vector<std::optional<ReceiverFunction>>;
		using ReceiverArray = std::array<ReceiverVector, TypeCount>;
		using IndexArray = std::array<int, TypeCount>;

		static ReceiverArray receiverArray;
		static IndexArray indexArray;

	public:

		static void Initialize();
		static int Subscribe(MessageTypes messageType, const ReceiverFunction& action);
		static void Unsubscribe(MessageTypes messageType, int index);
		static void Send(MessageTypes messageType, const std::any& data, float dt = 0);
		static void ProcessChanges();
	};
}
