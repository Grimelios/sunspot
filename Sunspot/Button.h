#pragma once
#include "Container2D.h"
#include "ISelectable.h"
#include "Sprite.h"
#include <functional>

namespace Sunspot
{
	class Button : public Container2D, public ISelectable
	{
	protected:

		Sprite icon;

	public:

		Button(const std::string& texture, const Bounds& source);
		Button(const Texture2D& texture, const Bounds& source);

		std::function<void()> clickFunction;

		void OnHover() override = 0;
		void OnUnhover() override = 0;
		void OnSelect() override;
		bool Contains(const glm::vec2& mousePosition) override;
		void SetLocation(const glm::ivec2& location) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
