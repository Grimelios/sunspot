#pragma once
#include "IRenderable.h"
#include "Curve.h"

namespace Sunspot
{
	class CurveVisualizer
	{
	private:

		const Curve& curve;

	public:

		explicit CurveVisualizer(const Curve& curve);

		void Draw(PrimitiveBatch& pb) const;
	};
}
