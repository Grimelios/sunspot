#include "Sword.h"
#include "DirectionalHurtbox.h"
#include "GameFunctions.h"

namespace Sunspot
{
	Sword::Sword(Entity* parent) : DamageSource(parent),
		slashShape(150, 1),
		activeShape(&slashShape)
	{
		damage = 3;
		knockback = 5;
	}

	Shape* Sword::ActiveShape() const
	{
		return activeShape;
	}

	bool Sword::HurtboxActive() const
	{
		return hurtbox != nullptr;
	}

	void Sword::TriggerAttack(const glm::vec2& direction, Entity* source)
	{
		const float rotation = GameFunctions::ComputeAngle(direction);

		activeShape = &slashShape;
		activeShape->rotation = rotation;

		hurtbox = std::make_unique<DirectionalHurtbox>(activeShape, direction, this);
		timerCollection.timers.Add(attackLifetime, [this](const float time)
		{
			hurtbox->Dispose();
			hurtbox.reset();
		});
	}
}
