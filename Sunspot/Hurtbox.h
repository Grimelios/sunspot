#pragma once
#include <vector>
#include "ITargetable.h"
#include "Shape.h"
#include "Sensor.h"
#include "DamageSource.h"

namespace Sunspot
{
	// Note that "box" doesn't strictly refer to boxes, but any generic shape.
	class Hurtbox : public ISensitive, public IDisposable
	{
	protected:

		std::vector<ITargetable*> targetsHit;

		// Outside classes must own the shape used by the hurtbox (commonly weapon classes).
		Sensor sensor;
		DamageSource* source;

		// This function is called apply "hit" because attacks can cause effects other than pure damage. The function
		// exists in the first place so that different kinds of hurtboxes can apply damage in different ways.
		virtual void ApplyHit(int damage, int knockback, ITargetable* target) = 0;

	public:

		Hurtbox(Shape* shape, DamageSource* source);
		virtual ~Hurtbox() = default;

		void OnSense(SensorTypes sensorType, ISensitive* data) override;
		void OnSeparation(SensorTypes sensorType, ISensitive* data) override;
		void Dispose() override;
	};
}
