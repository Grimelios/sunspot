#include "KeyboardData.h"
#include <utility>

namespace Sunspot
{
	KeyboardData::KeyboardData(std::vector<int> keysHeld, std::vector<KeyPress> keysPressedThisFrame,
		std::vector<int> keysReleasedThisFrame, const KeyArray& keyArray) :
		
		keysHeld(std::move(keysHeld)),
		keysPressedThisFrame(std::move(keysPressedThisFrame)),
		keysReleasedThisFrame(std::move(keysReleasedThisFrame)),
		keyArray(keyArray)
	{
	}

	const std::vector<int>& KeyboardData::KeysHeld() const
	{
		return keysHeld;
	}

	const std::vector<KeyPress>& KeyboardData::KeysPressedThisFrame() const
	{
		return keysPressedThisFrame;
	}

	const std::vector<int>& KeyboardData::KeysReleasedThisFrame() const
	{
		return keysReleasedThisFrame;
	}

	bool KeyboardData::Query(const std::any& data, const InputStates state) const
	{
		const int key = std::any_cast<int>(data);

		return (keyArray[key] & state) == state;
	}
}
