#pragma once
#include <string>

namespace Sunspot
{
	class ItemData
	{
	private:

		std::string name;
		std::string description;

		int value;

	public:


		ItemData(std::string name, std::string description, int value);

		const std::string& Name() const;
		const std::string& Description() const;

		int Value() const;
	};
}
