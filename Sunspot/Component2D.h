#pragma once
#include "IRenderable.h"
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include "Enumerations.h"

namespace Sunspot
{
	class Component2D : public IRenderable
	{
	protected:

		Alignments alignment;

		glm::ivec2 origin = glm::ivec2(0);

		explicit Component2D(Alignments alignment);

	public:

		glm::vec2 position = glm::vec2(0);
		glm::vec2 scale = glm::vec2(1);
		glm::vec4 color = glm::vec4(1);

		float rotation = 0;

		SpriteModifiers mods = SpriteModifiers::None;

		void Opacity(float opacity);
	};
}
