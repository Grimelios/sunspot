#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	class Container2D : public IDynamic, public IRenderable
	{
	protected:

		Bounds bounds;

		bool centered = false;

	public:

		virtual ~Container2D() = 0;

		glm::ivec2 GetLocation() const;

		// The reason that bounds aren't simply exposed publicly is that bounds functions (width, height, and location) can
		// be overridden. To allow direct modification of the bounds might risk accidentally changing the bounds without
		// updating the container as a whole.
		const Bounds& GetBounds() const;

		virtual void SetWidth(int width);
		virtual void SetHeight(int height);
		virtual void SetLocation(const glm::ivec2& location);
		
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};

	inline Container2D::~Container2D() = default;
}
