#pragma once
#include "Bounds.h"

namespace Sunspot
{
	class Resolution
	{
	public:

		static int Width;
		static int Height;

		// During normal gameplay, target bounds will be equivalent to window bounds, but it's still useful for rendering
		// gameplay in a smaller render target (such as in the editor).
		static Bounds Target;
	};
}
