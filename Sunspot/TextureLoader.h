#pragma once
#include "Texture2D.h"
#include <string>

namespace Sunspot::TextureLoader
{
	Texture2D Load(const std::string& filename);
}
