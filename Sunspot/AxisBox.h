#pragma once
#include "Shape.h"
#include <glm/vec2.hpp>
#include <array>

namespace Sunspot
{
	class AxisBox : public Shape
	{
	public:

		AxisBox();
		explicit AxisBox(int size);
		AxisBox(int width, int height);
		AxisBox(int width, int height, const glm::vec2& center);

		int width;
		int height;

		std::array<glm::vec2, 4> corners;

		void ComputeCorners();

		bool Contains(const glm::vec2& point);
	};
}
