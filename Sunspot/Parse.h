#pragma once
#include <glm/vec2.hpp>
#include <string>

namespace Sunspot::Parse
{
	glm::vec2 Vec2(const std::string& value);
}
