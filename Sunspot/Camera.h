#pragma once
#include "IDynamic.h"
#include <glm/vec2.hpp>
#include <glm/mat3x3.hpp>

namespace Sunspot
{
	enum class CameraModes
	{
		Fixed,
		Follow,
		Free,
		Unassigned
	};

	class Camera : public IDynamic
	{
	private:

		glm::mat4 view;

	public:

		glm::vec2 position = glm::vec2(0);
		glm::ivec2 origin = glm::ivec2(0);

		float zoom = 1;
		float rotation = 0;

		CameraModes mode = CameraModes::Unassigned;

		const glm::mat4& View() const;

		void Update(float dt) override;
	};
}
