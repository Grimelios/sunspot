#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <glm/vec4.hpp>
#include <array>
#include <memory>
#include "EditorStatusBar.h"
#include "EditorTool.h"
#include "EditorToolbar.h"
#include "TilePanel.h"
#include "SelectableSet.h"
#include "CameraTool.h"
#include "SavePanel.h"
#include "Scene.h"

namespace Sunspot
{
	class EditorToolbar;
	class Editor : public IDynamic, public IRenderable
	{
	private:

		using ToolArray = std::array<std::unique_ptr<EditorTool>, 2>;

		Bounds toolboxBounds;
		Bounds fullBounds;
		Bounds gameplayBounds;

		int borderSize = 4;

		glm::vec4 backgroundColor;
		glm::vec4 borderColor;

		SelectableSet selectableSet;
		ToolArray toolArray;
		Camera& camera;
		CameraTool* cameraTool;
		EditorTool* activeTool;
		EditorPanel* activePanel;
		EditorStatusBar statusBar;

		std::unique_ptr<EditorToolbar> toolbar;

		bool mouseWithinGameplayBounds = false;
		bool active = false;

		int messagingKeyboardIndex = -1;
		int messagingMouseIndex = -1;

		void HandleKeyboard(const KeyboardData& data);
		void HandleMouse(const MouseData& data);

	public:

		explicit Editor(Camera& camera);

		Bounds GetGameplayBounds() const;
		EditorTool* ActiveTool() const;

		bool Active() const;
		bool MouseWithinGameplayBounds() const;

		void Activate();
		void Deactivate();
		void Reload(Scene& scene);
		void SelectTool(int index);
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
