#include "SensorWorld.h"
#include <vector>
#include "VectorUtilities.h"

namespace Sunspot
{
	const SensorWorld::SensorArray& SensorWorld::Sensors() const
	{
		return sensors;
	}

	void SensorWorld::Add(Sensor* sensor)
	{
		const int index = static_cast<int>(sensor->GetSensorType());

		sensor->node = sensors[index].Add(sensor);
	}

	void SensorWorld::Remove(Sensor* sensor)
	{
		const int index = static_cast<int>(sensor->GetSensorType());

		sensors[index].Remove(sensor->node);
	}

	void SensorWorld::Update(const float dt)
	{
		for (SensorList& list : sensors)
		{
			auto node = list.Head();

			while (node != nullptr)
			{
				Sensor& sensor = *node->data;
				node = node->next;

				if (!sensor.enabled || sensor.canActivate.empty())
				{
					continue;
				}

				const SensorTypes sensorType = sensor.GetSensorType();

				Shape& shape = *sensor.GetShape();
				ISensitive* parent = sensor.GetParent();

				std::vector<Sensor*> contactList = sensor.contactList;

				for (Sensor* contact : contactList)
				{
					if (!helper.CheckOverlap(shape, *contact->GetShape()))
					{
						ISensitive* contactParent = contact->GetParent();

						// Although sensor activation is one-directional, separation is called on both sides.
						parent->OnSeparation(contact->GetSensorType(), contactParent);
						contactParent->OnSeparation(sensorType, parent);
					}
				}

				for (const SensorTypes type : sensor.canActivate)
				{
					auto& list2 = sensors[static_cast<int>(type)];
					auto node2 = list2.Head();

					while (node2 != nullptr)
					{
						Sensor* other = node2->data;

						if (&sensor != other &&	!VectorUtilities::Contains(contactList, other) &&
							helper.CheckOverlap(shape, *other->GetShape()))
						{
							other->GetParent()->OnSense(sensorType, parent);
							sensor.contactList.push_back(other);
						}

						node2 = node2->next;
					}
				}
			}
		}
	}
}
