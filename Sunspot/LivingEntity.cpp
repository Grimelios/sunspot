#include "LivingEntity.h"

namespace Sunspot
{
	void LivingEntity::OnDeath()
	{
	}

	void LivingEntity::RegisterHit(const int damage, const int knockback, const glm::vec2& direction, Entity* source)
	{
		health -= damage;

		if (health <= 0)
		{
			health = 0;
			OnDeath();
		}
	}

	void LivingEntity::Kill()
	{
		OnDeath();
	}
}
