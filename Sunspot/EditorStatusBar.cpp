#include "EditorStatusBar.h"

namespace Sunspot
{
	EditorStatusBar::EditorStatusBar() :
		statusText("Editor", std::nullopt, Alignments::Left)
	{
	}

	void EditorStatusBar::SetLocation(const glm::ivec2& location)
	{
		statusText.position = glm::vec2(location.x + XOffset, location.y + bounds.height / 2);
		bounds.SetLocation(location);
	}

	void EditorStatusBar::Append(const std::string& value)
	{
		const std::optional<std::string>& existingValue = statusText.Value();

		if (!existingValue.has_value())
		{
			statusText.Value(value);
		}
		else
		{
			statusText.Value(existingValue.value() + " | " + value);
		}
	}

	void EditorStatusBar::Clear()
	{
		statusText.Value(std::nullopt);
	}

	void EditorStatusBar::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		statusText.Draw(sb, pb);
	}
}
