#pragma once
#include "EntityFactory.h"
#include <vector>

namespace Sunspot
{
	class Scene;
	class Entity;
	class FragmentLoader
	{
	private:

		using Json = nlohmann::json;
		using EntityList = std::vector<std::unique_ptr<Entity>>;

		EntityFactory entityFactory;

	public:

		EntityList Load(const std::string& filename, Scene* scene);
	};
}
