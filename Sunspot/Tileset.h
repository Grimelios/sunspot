#pragma once
#include <string>

namespace Sunspot
{
	class Tileset
	{
	private:

		std::string filename;

		int tileWidth;
		int tileHeight;
		int tileSpacing;

	public:

		Tileset(std::string filename, int tileWidth, int tileHeight, int tileSpacing);

		const std::string& Filename() const;

		int TileWidth() const;
		int TileHeight() const;
		int TileSpacing() const;
	};
}
