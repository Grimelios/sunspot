#include "AxisBox.h"

namespace Sunspot
{
	AxisBox::AxisBox() : AxisBox(0, 0, glm::vec2(0))
	{
	}

	AxisBox::AxisBox(const int size) : AxisBox(size, size, glm::vec2(0))
	{
	}

	AxisBox::AxisBox(const int width, const int height) : AxisBox(width, height, glm::vec2(0))
	{
	}

	AxisBox::AxisBox(const int width, const int height, const glm::vec2& center) :
		Shape(center, 0, ShapeTypes::AxisBox),

		width(width),
		height(height)
	{
	}

	bool AxisBox::Contains(const glm::vec2& point)
	{
		const float dX = std::abs(point.x - position.x);
		const float dY = std::abs(point.y - position.y);

		// To avoid problems with integer division, boxes should probably have even dimensions.
		return dX <= width / 2 && dY <= height / 2;
	}

	void AxisBox::ComputeCorners()
	{
		const int halfWidth = width / 2;
		const int halfHeight = height / 2;

		corners =
		{
			glm::vec2(-halfWidth, -halfHeight),
			glm::vec2(halfWidth, -halfHeight),
			glm::vec2(halfWidth, halfHeight),
			glm::vec2(-halfWidth, halfHeight)
		};

		for (glm::vec2& v : corners)
		{
			v += position;
		}
	}
}
