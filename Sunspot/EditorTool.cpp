#include "EditorTool.h"

namespace Sunspot
{
	EditorTool::EditorTool(std::string name) : name(std::move(name))
	{
	}

	const std::string& EditorTool::Name() const
	{
		return name;
	}

	EditorPanel* EditorTool::Panel() const
	{
		return panel.get();
	}

	void EditorTool::Reload(Scene& scene)
	{
	}

	void EditorTool::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
	}
}
