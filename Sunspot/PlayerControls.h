#pragma once
#include <vector>
#include "InputBind.h"
#include <nlohmann/json.hpp>

namespace Sunspot
{
	class PlayerControls
	{
	private:

		using Json = nlohmann::json;

		static std::vector<InputBind> ParseBinds(const Json& j);

	public:

		PlayerControls();

		std::vector<InputBind> up;
		std::vector<InputBind> down;
		std::vector<InputBind> left;
		std::vector<InputBind> right;
		std::vector<InputBind> attack;
		std::vector<InputBind> interact;
	};
}
