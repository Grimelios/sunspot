#include "CurveVisualizer.h"
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

namespace Sunspot
{
	CurveVisualizer::CurveVisualizer(const Curve& curve) : curve(curve)
	{
	}

	void CurveVisualizer::Draw(PrimitiveBatch& pb) const
	{
		auto points = curve.Evaluate<30>();

		for (int i = 0; i < static_cast<int>(points.size()) - 1; i++)
		{
			pb.DrawLine(points[i], points[i + 1], glm::vec4(0.4f, 0.4f, 0.4f, 1.0f));
		}

		for (const glm::vec2& point : curve.controlPoints)
		{
			pb.DrawPoint(point, glm::vec4(1));
		}
	}
}
