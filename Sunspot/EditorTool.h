#pragma once
#include <string>
#include "MouseData.h"
#include "IRenderable.h"
#include "EditorStatusBar.h"
#include "EditorPanel.h"
#include <memory>
#include "Scene.h"

namespace Sunspot
{
	class EditorTool : IRenderable
	{
	private:

		std::string name;

	protected:

		std::unique_ptr<EditorPanel> panel = nullptr;

		explicit EditorTool(std::string name);

	public:

		const std::string& Name() const;

		// Setting the status bar through a pointer simplifies tool construction.
		EditorStatusBar* statusBar = nullptr;
		EditorPanel* Panel() const;

		virtual void Reload(Scene& scene);
		virtual void HandleMouse(const MouseData& data) = 0;

		// Note that this function refers to drawing world-space components (usually while using the tool).
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
