#include "DataClass.h"
#include "JsonUtilities.h"

namespace Sunspot
{
	DataClass::Json DataClass::Load(const std::string& filename) const
	{
		return JsonUtilities::Load("Data/" + filename);
	}
}
