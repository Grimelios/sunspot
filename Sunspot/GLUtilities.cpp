#include "GLUtilities.h"

namespace Sunspot
{
	GLuint GLUtilities::CreateTarget(const int width, const int height)
	{
		// See http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-14-render-to-texture/.
		GLuint framebuffer;

		glGenFramebuffers(1, &framebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

		GLuint texture;

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		GLuint depthBuffer;

		glGenRenderbuffers(1, &depthBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);

		GLenum drawBuffers[1] = { GL_COLOR_ATTACHMENT0 };

		glDrawBuffers(1, drawBuffers);

		// It's simpler to always reset the render target back to zero here.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		return framebuffer;
	}
}
