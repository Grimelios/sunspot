#include "EditorToolbar.h"
#include <array>
#include "ContentCache.h"
#include "EditorConstants.h"

namespace Sunspot
{
	EditorToolbar::EditorToolbar(SelectableSet& parentSet, Editor* parent) :
		parent(parent)
	{
		const Texture2D& texture(ContentCache::GetTexture("UI/Editor.png"));
		const std::array<std::string, 2> names =
		{
			"Camera",
			"Tiles"
		};

		for (int i = 0; i < static_cast<int>(names.size()); i++)
		{
			icons[i] = std::make_unique<EditorToolbarIcon>(texture, names[i], i, this);
			parentSet.items.push_back(icons[i].get());
		}

		EditorToolbarIcon& icon = *icons[activeIndex];
		icon.OnHover();
		icon.selected = true;
	}

	void EditorToolbar::SetActive(const int index)
	{
		EditorToolbarIcon& previousIcon = *icons[activeIndex];
		previousIcon.selected = false;
		previousIcon.OnUnhover();

		activeIndex = index;
		parent->SelectTool(index);
	}

	void EditorToolbar::SetLocation(const glm::ivec2& location)
	{
		const int iconOffset = 2;

		const glm::ivec2 start = location + glm::ivec2(iconOffset);
		const glm::ivec2 spacing = glm::ivec2(EditorConstants::IconSize, 0);

		for (int i = 0; i < static_cast<int>(icons.size()); i++)
		{
			icons[i]->SetLocation(start + spacing * i);
		}

		bounds.SetLocation(location);
	}

	void EditorToolbar::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		for (std::unique_ptr<EditorToolbarIcon>& icon : icons)
		{
			icon->Draw(sb, pb);
		}
	}
}
