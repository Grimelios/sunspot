#include "TimerCollection.h"

namespace Sunspot
{
	void TimerCollection::Update(const float dt)
	{
		if (timers.IsEmpty())
		{
			return;
		}

		DoublyLinkedListNode<Timer>* node = timers.Head();

		while (node != nullptr)
		{
			Timer& timer = node->data;
			timer.Update(dt);

			DoublyLinkedListNode<Timer>* next = node->next;

			if (timer.Completed())
			{
				timers.Remove(node);
			}

			node = next;
		}
	}
}
