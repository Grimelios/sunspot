#pragma once

namespace Sunspot
{
	class EditorConstants
	{
	public:

		static const int IconSize = 32;
		static const int HalfIconSize = IconSize / 2;
		static const int Padding = 2;

		static const float InactiveOpacity;
	};
}
