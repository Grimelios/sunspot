#pragma once
#include "Hurtbox.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	class DirectionalHurtbox : public Hurtbox
	{
	private:

		glm::vec2 direction;

	protected:

		void ApplyHit(int damage, int knockback, ITargetable* target) override;

	public:

		DirectionalHurtbox(Shape* shape, const glm::vec2& direction, DamageSource* source);
	};
}
