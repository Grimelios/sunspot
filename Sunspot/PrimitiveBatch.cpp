#include "PrimitiveBatch.h"
#include "GLUtilities.h"
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Sunspot
{
	PrimitiveBatch::PrimitiveBatch(const Camera& camera) : BatchBase(camera),
		shader("Primitives"),
		program(shader.Program()),
		lineGroup(GL_LINES),
		pointGroup(GL_POINTS),
		triangleGroup(GL_TRIANGLES),
		pointIndices({ 0 }),
		lineIndices({ 0, 1 }),
		boundsOutlineIndices({ 0, 1, 1, 2, 2, 3, 3, 0 }),
		quadIndices({ 0, 1, 2, 0, 2, 3 })
	{
		glGenBuffers(1, &positionBuffer);
		glGenBuffers(1, &colorBuffer);
		glGenBuffers(1, &indexBuffer);
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(glm::vec3), nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
		glVertexAttribPointer(1, 4, GL_FLOAT, false, sizeof(glm::vec4), nullptr);

		for (GLuint i = 0; i < 2; i++)
		{
			glEnableVertexAttribArray(i);
		}

		flipLocation = glGetUniformLocation(program, "flip");
		mvpLocation = glGetUniformLocation(program, "mvp");

		// See the note in SpriteBatch about setting this value.
		glUseProgram(program);
		glUniform1i(flipLocation, -1);
	}

	void PrimitiveBatch::SetOther(SpriteBatch* sb)
	{
		this->sb = sb;
	}

	void PrimitiveBatch::Flip(const int flip) const
	{
		glUseProgram(program);
		glUniform1i(flipLocation, flip);
	}

	void PrimitiveBatch::DrawPoint(const glm::vec2& point, const glm::vec4& color)
	{
		std::array<glm::vec2, 1> vertex = { point };

		PushData(pointGroup, vertex, color, pointIndices);
	}

	void PrimitiveBatch::DrawLine(const Line& line, const glm::vec4& color)
	{
		DrawLine(line.start, line.end, color);
	}

	void PrimitiveBatch::DrawLine(const glm::vec2& start, const glm::vec2& end, const glm::vec4& color)
	{
		if (!drawing)
		{
			throw std::runtime_error("Batch must be started before drawing.");
		}

		const std::array<glm::vec2, 2> vertices = { start, end };

		PushData(lineGroup, vertices, color, lineIndices);
	}

	void PrimitiveBatch::DrawBounds(const Bounds& bounds, const glm::vec4& color)
	{
		if (!drawing)
		{
			throw std::runtime_error("Batch must be started before drawing.");
		}

		PushData(lineGroup, bounds.GetCorners(), color, boundsOutlineIndices);
	}

	void PrimitiveBatch::FillBounds(const Bounds& bounds, const glm::vec4& color)
	{
		if (!drawing)
		{
			throw std::runtime_error("Batch must be started before drawing.");
		}

		PushData(triangleGroup, bounds.GetCorners(), color, quadIndices);
	}

	void PrimitiveBatch::Flush(const BatchCoords coords)
	{
		const glm::mat4 mvp = coords == BatchCoords::Screen ? screenMatrix : camera.View() * projection;

		glUseProgram(program);
		glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, value_ptr(mvp));
		glBindVertexArray(vao);

		Flush(lineGroup);
		Flush(pointGroup);
		Flush(triangleGroup);
	}

	void PrimitiveBatch::Flush(PrimitiveGroup& group) const
	{
		if (group.indices.empty())
		{
			return;
		}

		GLUtilities::BufferData(positionBuffer, group.vertices, sizeof(glm::vec3));
		GLUtilities::BufferData(colorBuffer, group.colors, sizeof(glm::vec4));

		const std::vector<int>& indices = group.indices;

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
		glDrawElements(group.Mode(), indices.size(), GL_UNSIGNED_INT, nullptr);

		group.Clear();
	}
}
