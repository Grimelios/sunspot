#include "SelectableSet.h"

namespace Sunspot
{
	void SelectableSet::HandleMouse(const MouseData& data)
	{
		const glm::ivec2& mousePosition = data.ScreenPosition();

		if (hoveredItem != nullptr && !hoveredItem->Contains(mousePosition))
		{
			hoveredItem->OnUnhover();
			hoveredItem = nullptr;
		}

		for (ISelectable* item : items)
		{
			if (item != hoveredItem && item->Contains(mousePosition))
			{
				hoveredItem = item;
				hoveredItem->OnHover();

				// This assumes that hoverable items won't overlap.
				break;
			}
		}

		if (hoveredItem != nullptr && data.Query(GLFW_MOUSE_BUTTON_LEFT, InputStates::PressedThisFrame))
		{
			hoveredItem->OnSelect();
		}
	}
}
