#include "ShapeVisualizer.h"
#include "GameFunctions.h"
#include <glm/vec4.hpp>

namespace Sunspot
{
	ShapeVisualizer::ShapeVisualizer(const SensorWorld& sensorWorld) :
		sensors(sensorWorld.Sensors())
	{
	}

	void ShapeVisualizer::Draw(PrimitiveBatch& pb) const
	{
		for (const SensorWorld::SensorList& list : sensors)
		{
			const SensorWorld::SensorNode* node = list.Head();

			while (node != nullptr)
			{
				DrawShape(*node->data->GetShape(), pb);
				node = node->next;
			}
		}
	}

	void ShapeVisualizer::DrawShape(Shape& shape, PrimitiveBatch& pb) const
	{
		switch (shape.ShapeType())
		{
			case ShapeTypes::AxisBox: DrawAxisBox(static_cast<AxisBox&>(shape), pb); break;
			case ShapeTypes::Sector: DrawSector(static_cast<Sector&>(shape), pb); break;
		}
	}

	void ShapeVisualizer::DrawAxisBox(AxisBox& shape, PrimitiveBatch& pb) const
	{
		Bounds bounds(shape.width, shape.height);
		bounds.SetCenter(shape.position);

		pb.DrawBounds(bounds, color);
	}

	void ShapeVisualizer::DrawSector(Sector& sector, PrimitiveBatch& pb) const
	{
		const glm::vec2& center = sector.position;

		const float spread = sector.spread;
		const float halfSpread = spread / 2;
		const float radius = static_cast<float>(sector.radius);

		const int segmentCount = static_cast<int>(std::floor(sector.spread * radius / MaxRadialSegmentLength)) + 1;

		std::vector<glm::vec2> points;
		points.reserve(segmentCount + 1);

		const float start = sector.rotation - halfSpread;
		const float increment = spread / segmentCount;
;
		for (int i = 0; i <= segmentCount; i++)
		{
			const glm::vec2 direction = GameFunctions::ComputeDirection(start + increment * i);
			points.emplace_back(center + direction * radius);
		}

		pb.DrawLine(center, points[0], color);
		pb.DrawLine(center, points.back(), color);

		for (int i = 0; i < segmentCount; i++)
		{
			pb.DrawLine(points[i], points[i + 1], color);
		}
	}
}
