#pragma once
#include <glm/vec2.hpp>
#include "Entity.h"

namespace Sunspot
{
	class ITargetable
	{
	public:

		virtual ~ITargetable() = default;
		virtual void RegisterHit(int damage, int knockback, const glm::vec2& direction, Entity* source) = 0;
	};
}
