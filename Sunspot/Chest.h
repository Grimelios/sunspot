#pragma once
#include "Mechanism.h"
#include "Sprite.h"

namespace Sunspot
{
	class Chest : public Mechanism
	{
	private:

		Sprite sprite;

	protected:

		void Activate(Player& player) override;

	public:

		Chest();
	};
}
