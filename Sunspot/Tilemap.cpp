#include "Tilemap.h"
#include "ContentCache.h"
#include "AxisBox.h"

namespace Sunspot
{
	Tilemap::Tilemap(const std::string& filename) : Tilemap(TilemapData(filename))
	{
	}

	Tilemap::Tilemap(const TilemapData& data) : render(data)
	{
		renderBounds = render.GetBounds();
		shape = std::make_unique<AxisBox>(renderBounds.width, renderBounds.height);
	}

	void Tilemap::SetPosition(const glm::vec2& position)
	{
		// Note that not calling the base function is intentional here, on the assumption that tilemaps contain no
		// components and don't use sensors.
		this->position = position;

		render.SetPosition(position);
		renderBounds.SetLocation(position);
	}

	void Tilemap::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		render.Draw(sb, pb);
	}
}
