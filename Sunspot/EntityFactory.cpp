#include "EntityFactory.h"
#include "Octoguard.h"
#include "BigTentacle.h"
#include "ShipGate.h"

namespace Sunspot
{
	EntityFactory::EntityFactory() :
		types(
		{
			{ "BigTentacle", EntityTypes::BigTentacle },
			{ "Octoguard", EntityTypes::Octoguard },
			{ "ShipGate", EntityTypes::ShipGate },
			{ "ShipWheel", EntityTypes::ShipWheel },
			{ "Tilemap", EntityTypes::Tilemap }
		})
	{
	}

	std::unique_ptr<Entity> EntityFactory::Create(const Json& j, Scene* scene)
	{
		const std::string type = j.at("Type").get<std::string>();

		switch (types.at(type))
		{
			case EntityTypes::BigTentacle: return std::make_unique<BigTentacle>(j);
			case EntityTypes::Octoguard: return std::make_unique<Octoguard>(j);
			case EntityTypes::ShipGate: return std::make_unique<ShipGate>(j, scene);
			case EntityTypes::ShipWheel: return std::make_unique<ShipWheel>(j, scene);
		}

		return nullptr;
	}
}
