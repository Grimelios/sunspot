#pragma once
#include "Entity.h"

namespace Sunspot
{
	class DamageSource : public Entity
	{
	private:

		Entity* parent;

	protected:

		int damage = 0;
		int knockback = 0;

	public:

		explicit DamageSource(Entity* parent);
		virtual ~DamageSource() = 0;

		int Damage() const;
		int Knockback() const;

		Entity* Parent() const;
	};

	inline DamageSource::~DamageSource() = default;
}
