#include "ShipGateData.h"

namespace Sunspot
{
	ShipGateData::ShipGateData()
	{
		Json j = Load("ShipGateData.json");

		distance = j.at("Distance").get<int>();
	}

	int ShipGateData::Distance() const
	{
		return distance;
	}
}
