#include "SpriteBatch.h"
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "VectorUtilities.h"
#include "GLUtilities.h"
#include "Messaging.h"
#include "MapUtilities.h"
#include "Resolution.h"
#include "Sprite.h"

namespace Sunspot
{
	SpriteBatch::SpriteBatch(const Camera& camera) : BatchBase(camera),
		spriteShader("Sprite"),
		program(spriteShader.Program()),
		quadTexCoords(
			{
				glm::vec2(0.0f),
				glm::vec2(1.0f, 0.0f),
				glm::vec2(1.0f),
				glm::vec2(0.0f, 1.0f)
			}),

		quadIndices(
			{
				0, 1, 2, 0, 2, 3
			})
	{
		glGenBuffers(1, &positionBuffer);
		glGenBuffers(1, &texCoordBuffer);
		glGenBuffers(1, &colorBuffer);
		glGenBuffers(1, &indexBuffer);
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordBuffer);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::vec4), nullptr);

		flipLocation = glGetUniformLocation(program, "flip");
		mvpLocation = glGetUniformLocation(program, "mvp");

		// Uniform values are reset after the shader is linked (based on my research), so it needs to be initially set
		// here.
		glUseProgram(program);
		glUniform1i(flipLocation, -1);

		for (GLuint i = 0; i < 3; i++)
		{
			glEnableVertexAttribArray(i);
		}

		activeMap = &MapUtilities::Add(spriteMap, &spriteShader, InnerMap());
	}

	void SpriteBatch::SetOther(PrimitiveBatch* pb)
	{
		this->pb = pb;
	}

	void SpriteBatch::SetTarget(const RenderTarget* target) const
	{
		if (drawing)
		{
			throw std::runtime_error("The batch must be ended before switching render targets.");
		}

		glUseProgram(program);

		// Note that I intentionally don't call glViewport. In practice, I don't want textures to stretch, even for
		// smaller render targets.
		if (target == nullptr)
		{
			// The main screen must be flipped vertically, but render targets shouldn't be.
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glUniform1i(flipLocation, -1);

			pb->Flip(-1);
		}
		else
		{
			glBindFramebuffer(GL_FRAMEBUFFER, target->FrameBuffer());
			glUniform1i(flipLocation, 1);

			pb->Flip(1);

			// This assumes that targets will only be used once per frame. This could be updated later to clear based
			// on a function argument.
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
	}

	void SpriteBatch::Apply(const Shader& shader)
	{
	}

	void SpriteBatch::Draw(const Texture2D& texture, const glm::vec2& position)
	{
		Draw(texture, std::nullopt, position, glm::ivec2(0), glm::vec4(1), 0, glm::vec2(1), SpriteModifiers::None);
	}

	void SpriteBatch::Draw(const Texture2D& texture, const std::optional<Bounds>& source, const glm::vec2& position)
	{
		Draw(texture, source, position, glm::ivec2(0), glm::vec4(1), 0, glm::vec2(1), SpriteModifiers::None);
	}

	void SpriteBatch::Draw(const Texture2D& texture, const std::optional<Bounds>& source, const glm::vec2& position,
		const glm::ivec2& origin, const glm::vec4& color, const float rotation, const glm::vec2& scale,
		const SpriteModifiers mods)
	{
		Draw(texture.TextureId(), texture.Width(), texture.Height(), source, position, origin, color, rotation, scale, mods);
	}

	void SpriteBatch::Draw(const RenderTarget& target, const glm::vec2& position)
	{
		Draw(target.TextureId(), target.Width(), target.Height(), std::nullopt, position, glm::ivec2(0), glm::vec4(1),
			0, glm::vec2(1), SpriteModifiers::None);
	}

	void SpriteBatch::Draw(const GLuint textureId, const int width, const int height, const std::optional<Bounds>& source,
		glm::vec2 position, glm::ivec2 origin, const glm::vec4& color, const float rotation, const glm::vec2& scale,
		const SpriteModifiers mods)
	{
		if (!drawing)
		{
			throw std::runtime_error("Batch must be started before drawing.");
		}

		VerifyTexture(textureId);

		glm::vec2 dimensions;
		std::array<glm::vec2, 4> sourceCoords { };

		// Adding texture coordinates in here as well allows the source bounds to only be used once (if given).
		if (source.has_value())
		{
			const Bounds& bounds = source.value();

			const int right = bounds.x + bounds.width;
			const int bottom = bounds.y + bounds.height;

			sourceCoords =
			{
				glm::vec2(bounds.x, bounds.y),
				glm::vec2(right, bounds.y),
				glm::vec2(right, bottom),
				glm::vec2(bounds.x, bottom)
			};

			for (glm::vec2& v : sourceCoords)
			{
				v.x /= width;
				v.y /= height;
			}

			dimensions.x = static_cast<float>(bounds.width);
			dimensions.y = static_cast<float>(bounds.height);
		}
		else
		{
			sourceCoords = quadTexCoords;
			dimensions.x = static_cast<float>(width);
			dimensions.y = static_cast<float>(height);
		}

		if ((mods & SpriteModifiers::FlipHorizontal) > 0)
		{
			std::swap(sourceCoords[0], sourceCoords[1]);
			std::swap(sourceCoords[2], sourceCoords[3]);
		}

		VectorUtilities::PushArray(activeGroup->texCoords, sourceCoords);

		std::array<glm::vec2, 4> vertexArray
		{
			glm::vec2(0),
			glm::vec2(dimensions.x, 0.0f),
			dimensions,
			glm::vec2(0.0f, dimensions.y)
		};

		origin.x = static_cast<int>(origin.x * scale.x);
		origin.y = static_cast<int>(origin.y * scale.y);

		for (glm::vec2& v : vertexArray)
		{
			v -= origin;
		}

		if (rotation != 0)
		{
			for (int i = 0; i < 4; i++)
			{
				const glm::mat4 rotationMatrix = rotate(glm::mat4(1.0f), rotation, glm::vec3(0, 0, 1));
				const glm::vec4 v = rotationMatrix * glm::vec4(vertexArray[i], 0, 1);

				vertexArray[i] = glm::vec2(v.x, v.y);
			}
		}

		// Flooring to integer values reduces blurriness.
		position.x = floor(position.x);
		position.y = floor(position.y);

		for (glm::vec2& v : vertexArray)
		{
			v += position;
		}

		std::array<glm::vec3, 4> vertices3D{};

		for (int i = 0; i < 4; i++)
		{
			vertices3D[i] = glm::vec3(vertexArray[i], zValue);
		}

		VectorUtilities::PushArray(activeGroup->vertices, vertices3D);
		VectorUtilities::Repeat(activeGroup->colors, color, 4);

		std::vector<int>& indices = activeGroup->indices;

		int& indexOffset = activeGroup->indexOffset;

		for (int i = 0; i < 6; i++)
		{
			indices.push_back(indexOffset + quadIndices[i]);
		}

		indexOffset += 4;

		zValue -= ZIncrement;
		pb->zValue = zValue;
	}

	void SpriteBatch::DrawString(const SpriteFont& font, const std::string& value, const glm::vec2& position,
		const glm::ivec2& origin, const float rotation, const glm::vec4& color, const glm::vec2& scale)
	{
		if (!drawing)
		{
			throw std::runtime_error("Batch must be started before drawing.");
		}

		VerifyTexture(font.Texture().TextureId());

		// Casting position to integer values reduces blurriness.
		glm::vec2 localPosition = -glm::vec2(origin);
		localPosition.x += static_cast<int>(position.x);
		localPosition.y += static_cast<int>(position.y);

		const SpriteFont::CharacterArray& dataArray = font.DataArray();

		for (char c : value)
		{
			const CharacterData& data = dataArray[c].value();

			const int width = data.Width();
			const int height = data.Height();
			const int kerning = 0;

			const glm::vec2 start = localPosition + glm::vec2(data.Offset()) + glm::vec2(kerning, 0);
			const std::array<glm::vec2, 4> vertexArray =
			{
				start,
				start + glm::vec2(width, 0),
				start + glm::vec2(width, height),
				start + glm::vec2(0, height)
			};

			std::array<glm::vec3, 4> vertices3D;

			for (int i = 0; i < 4; i++)
			{
				vertices3D[i] = glm::vec3(vertexArray[i], zValue);
			}

			VectorUtilities::PushArray(activeGroup->vertices, vertices3D);
			VectorUtilities::PushArray(activeGroup->texCoords, data.TexCoords());

			std::vector<int>& indices = activeGroup->indices;

			int& indexOffset = activeGroup->indexOffset;

			// These groups of six indices could be pushed inside this loop or outside. Both options seem about the same.
			for (int i = 0; i < 6; i++)
			{
				indices.push_back(indexOffset + quadIndices[i]);
			}

			indexOffset += 4;

			zValue -= ZIncrement;
			localPosition.x += data.Advance();
		}

		VectorUtilities::Repeat(activeGroup->colors, color, value.length() * 4);

		zValue -= ZIncrement;
		pb->zValue = zValue;
	}

	void SpriteBatch::VerifyTexture(const GLuint textureId)
	{
		if (activeTextureId == 0 || activeTextureId != textureId)
		{
			activeTextureId = textureId;

			const auto i = activeMap->find(activeTextureId);

			if (i == activeMap->end())
			{
				activeGroup = &activeMap->insert(InnerMap::value_type(activeTextureId, SpriteGroup())).first->second;
			}
			else
			{
				activeGroup = &i->second;
			}
		}
	}

	void SpriteBatch::Flush(const BatchCoords coords)
	{
		const glm::mat4 mvp = coords == BatchCoords::Screen ? screenMatrix : camera.View() * projection;

		glUseProgram(program);
		glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, value_ptr(mvp));
		glBindVertexArray(vao);

		for (auto& pair : spriteMap)
		{
			glUseProgram(pair.first->Program());

			for (auto& innerPair : pair.second)
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, innerPair.first);

				Flush(innerPair.second);
			}
		}

		activeTextureId = 0;
		activeGroup = nullptr;
		activeMap = &spriteMap.at(&spriteShader);
	}

	void SpriteBatch::Flush(SpriteGroup& group) const
	{
		if (group.indices.empty())
		{
			return;
		}

		GLUtilities::BufferData(positionBuffer, group.vertices, sizeof(glm::vec3));
		GLUtilities::BufferData(texCoordBuffer, group.texCoords, sizeof(glm::vec2));
		GLUtilities::BufferData(colorBuffer, group.colors, sizeof(glm::vec4));

		const std::vector<int>& indices = group.indices;

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);

		group.Clear();
	}
}
