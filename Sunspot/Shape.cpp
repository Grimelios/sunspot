#include "Shape.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	Shape::Shape(const glm::vec2& position, const float rotation, const ShapeTypes type) :
		shapeType(type),
		position(position),
		rotation(rotation)
	{
	}

	ShapeTypes Shape::ShapeType() const
	{
		return shapeType;
	}
}
