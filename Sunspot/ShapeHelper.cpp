#include "ShapeHelper.h"
#include <valarray>

namespace Sunspot
{
	bool ShapeHelper::CheckOverlap(Shape& s1, Shape& s2) const
	{
		const ShapeTypes type1 = s1.ShapeType();
		const ShapeTypes type2 = s2.ShapeType();

		switch (type1)
		{
			case ShapeTypes::AxisBox:
			{
				AxisBox& axisBox = static_cast<AxisBox&>(s1);

				switch (type2)
				{
					case ShapeTypes::AxisBox: return CheckOverlap(axisBox, static_cast<AxisBox&>(s2));
					case ShapeTypes::Box: break;
					case ShapeTypes::Circle: break;
					case ShapeTypes::Line: break;
					case ShapeTypes::Sector: break;
				}
			}

			break;

			case ShapeTypes::Box: break;
			case ShapeTypes::Circle: break;
			case ShapeTypes::Line: break;
			case ShapeTypes::Sector:
			{
				Sector& sector = static_cast<Sector&>(s1);

				switch (type2)
				{
					case ShapeTypes::AxisBox: return CheckOverlap(static_cast<AxisBox&>(s2), sector);
					case ShapeTypes::Box: break;
					case ShapeTypes::Circle: break;
					case ShapeTypes::Line: break;
					case ShapeTypes::Sector: break;
				}
			}
			
				break;
		}

		return false;
	}

	bool ShapeHelper::CheckOverlap(AxisBox& a1, AxisBox& a2)
	{
		const float dX = std::abs(a1.position.x - a2.position.x);
		const float dY = std::abs(a1.position.y - a2.position.y);

		return dX <= (a1.width + a2.width) / 2 && dY <= (a1.height + a2.height) / 2;
	}

	bool ShapeHelper::CheckOverlap(AxisBox& a, Sector& s)
	{
		a.ComputeCorners();
		s.ComputeCorners();

		if (a.Contains(s.position) || a.Contains(s.lowerCorner) || a.Contains(s.upperCorner))
		{
			return true;
		}

		for (const glm::vec2& c : a.corners)
		{
			if (s.Contains(c))
			{
				return true;
			}
		}

		return false;
	}
}
