#pragma once
#include <map>
#include <string>
#include <glad.h>

namespace Sunspot
{
	class Shader
	{
	private:

		using UniformMap = std::map<std::string, GLint>;

		static void LoadShader(const std::string& filename, GLenum type, GLuint& shaderId);

		GLuint vShader = -1;
		GLuint fShader = -1;
		GLuint program = -1;

		UniformMap uniformMap;

		void CreateProgram();
		void GetUniforms();

	public:

		explicit Shader(const std::string& name);

		Shader(const std::string& vShader, const std::string& fShader);

		GLuint Program() const;
		GLint Uniforms(const std::string& name);
	};
}
