#include "CameraTool.h"

namespace Sunspot
{
	CameraTool::CameraTool(Camera& camera) : EditorTool("Camera"),
		camera(camera)
	{
	}

	void CameraTool::HandleMouse(const MouseData& data)
	{
		const InputStates state = data.State(GLFW_MOUSE_BUTTON_LEFT);
		const glm::vec2& screenPosition = data.ScreenPosition();

		if (state == InputStates::PressedThisFrame)
		{
			cameraAnchor = camera.position;
			screenAnchor = screenPosition;
		}
		else if (state == InputStates::Held)
		{
			camera.position = cameraAnchor - (screenPosition - screenAnchor);
		}
	}
}
