#include "FileUtilities.h"
#include <fstream>
#include <filesystem>

// I should be able to simply use std::filesystem based on the C++ 17 standard, but for whatever reason, it's not
// recognized.
namespace fs = std::experimental::filesystem;
namespace Sunspot
{
	std::string FileUtilities::ReadAllText(const std::string& filename)
	{
		// See https://stackoverflow.com/questions/195323/what-is-the-most-elegant-way-to-read-a-text-file-with-c/195350.
		std::ifstream stream(filename);

		return std::string(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());
	}

	std::vector<std::string> FileUtilities::GetFiles(const std::string& directory)
	{
		std::vector<std::string> files;

		// See https://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c/37494654#37494654.
		for (const auto& entry : fs::directory_iterator(directory))
		{
			files.push_back(entry.path().filename().generic_string());
		}

		return files;
	}


	void FileUtilities::SkipLine(std::ifstream& stream)
	{
		stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	void FileUtilities::SkipLines(std::ifstream& stream, const int count)
	{
		for (int i = 0; i < count; i++)
		{
			// See https://stackoverflow.com/questions/477408/ifstream-end-of-line-and-move-to-next-line.
			stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}
}
