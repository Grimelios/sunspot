#pragma once
#include <glm/vec2.hpp>

namespace Sunspot
{
	class IHoverable
	{
	public:

		virtual ~IHoverable() = default;
		virtual void OnHover() = 0;
		virtual void OnUnhover() = 0;

		virtual bool Contains(const glm::vec2& mousePosition) = 0;
	};
}
