#pragma once
#include "Entity.h"
#include "Hurtbox.h"
#include "Sector.h"
#include <glm/vec2.hpp>

namespace Sunspot
{
	class Sword : public DamageSource
	{
	private:

		int attackLifetime = 100;

		Sector slashShape;
		Shape* activeShape = nullptr;

		std::unique_ptr<Hurtbox> hurtbox = nullptr;

	public:

		explicit Sword(Entity* parent);

		Shape* ActiveShape() const;

		bool HurtboxActive() const;

		void TriggerAttack(const glm::vec2& direction, Entity* source);
	};
}
