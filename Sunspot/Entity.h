#pragma once
#include "IDynamic.h"
#include "IRenderable.h"
#include <vector>
#include "Component2D.h"
#include <glm/vec2.hpp>
#include "TimerCollection.h"
#include "Sensor.h"
#include "ISensitive.h"
#include "nlohmann/json.hpp"

namespace Sunspot
{
	class Scene;
	class Entity : public IDynamic, public IRenderable, public ISensitive, public IDisposable
	{
	private:

		// This alias is copied from Scene. The alias from Scene can't be used directly because of circular pointers.
		using EntityNode = DoublyLinkedListNode<std::unique_ptr<Entity>>;

	protected:

		// Constructing entities using Json blobs simplifies object creation by the scene.
		using Json = nlohmann::json;

		// Bounds aren't guaranteed to exactly (like, to the the pixel) bound the visuals of the entity, but they ARE
		// guaranteed to fully cover it. In other words, render bounds might be too big, but they'll never be too
		// small.
		Bounds renderBounds;
		TimerCollection timerCollection;

		glm::vec2 position = glm::vec2(0);
		glm::vec2 shapeOffset = glm::vec2(0);

		float rotation = 0;

		// Note that a list of components is used even though many entities will only contain a single component (such
		// as a sprite). Using this list means that even simple classes don't need to override a bunch of functions.
		std::vector<Component2D*> components;

		// This shape is only used for sensing. Collision detection is handled separately.
		std::unique_ptr<Shape> shape = nullptr;
		std::unique_ptr<Sensor> sensor = nullptr;

		// This function was created because of the common pattern of "create shape, then create sensor using that
		// shape".
		template<class T, class... Args>
		void CreateSensor(SensorTypes sensorType, Args&&... args);

		// Note that some entities (like the player) aren't spawned from Json files.
		virtual void Initialize(const Json& j);

	public:

		virtual ~Entity() = 0;

		glm::vec2 GetPosition() const;

		// This value might be removed later, to be replaced by a velocity value from a physics body.
		glm::vec2 velocity = glm::vec2(0);

		float GetRotation() const;

		const Bounds& GetRenderBounds() const;

		Scene* scene = nullptr;
		EntityNode* node = nullptr;

		virtual void SetPosition(const glm::vec2& position);
		virtual void SetRotation(float rotation);

		void OnSense(SensorTypes type, ISensitive* data) override;
		void OnSeparation(SensorTypes type, ISensitive* data) override;
		void Dispose() override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};

	inline Entity::~Entity() = default;

	template <class T, class ... Args>
	void Entity::CreateSensor(const SensorTypes sensorType, Args&&... args)
	{
		shape = std::make_unique<T>(args...);
		sensor = std::make_unique<Sensor>(sensorType, shape.get(), this);
	}
}
