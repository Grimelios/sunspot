#include "GameFunctions.h"

namespace Sunspot
{
	int GameFunctions::Sign(const float value)
	{
		// See https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c.
		return (0 < value) - (value < 0);
	}

	float GameFunctions::ComputeAngle(const glm::vec2& value)
	{
		return ComputeAngle(glm::vec2(0), value);
	}

	float GameFunctions::ComputeAngle(const glm::vec2& start, const glm::vec2& end)
	{
		const float dX = end.x - start.x;
		const float dY = end.y - start.y;

		return std::atan2(dY, dX);
	}

	float GameFunctions::LengthSquared(const glm::vec2& value)
	{
		return value.x * value.x + value.y * value.y;
	}

	std::string GameFunctions::ToString(const glm::vec2& value)
	{
		const int x = static_cast<int>(value.x);
		const int y = static_cast<int>(value.y);

		return std::to_string(x) + ", " + std::to_string(y);
	}

	std::string GameFunctions::ToString(const Bounds& bounds)
	{
		return std::to_string(bounds.x) + ", " + std::to_string(bounds.y) + ", " + std::to_string(bounds.width) +
			", " + std::to_string(bounds.height);
	}

	glm::vec2 GameFunctions::ComputeDirection(const float angle)
	{
		const float x = std::cos(angle);
		const float y = std::sin(angle);

		return glm::vec2(x, y);
	}

	glm::ivec2 GameFunctions::ComputeOrigin(const int width, const int height, Alignments alignment)
	{
		const int alignmentValue = static_cast<int>(alignment);

		const bool left = (alignmentValue & static_cast<int>(Alignments::Left)) > 0;
		const bool right = (alignmentValue & static_cast<int>(Alignments::Right)) > 0;
		const bool top = (alignmentValue & static_cast<int>(Alignments::Top)) > 0;
		const bool bottom = (alignmentValue & static_cast<int>(Alignments::Bottom)) > 0;

		const int x = left ? 0 : (right ? width : width / 2);
		const int y = top ? 0 : (bottom ? height : height / 2);

		return glm::ivec2(x, y);
	}
}
