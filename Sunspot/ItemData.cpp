#include "ItemData.h"
#include <utility>

namespace Sunspot
{
	ItemData::ItemData(std::string name, std::string description, const int value) :
		name(std::move(name)),
		description(std::move(description)),
		value(value)
	{
	}

	const std::string& ItemData::Name() const
	{
		return name;
	}

	const std::string& ItemData::Description() const
	{
		return description;
	}

	int ItemData::Value() const
	{
		return value;
	}
}
