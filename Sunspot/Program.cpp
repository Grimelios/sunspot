#include "SunspotGame.h"

int main(int argc, char* argv[])
{
	const int width = std::stoi(argv[1]);
	const int height = std::stoi(argv[2]);

	Sunspot::SunspotGame game(width, height);
	game.Initialize();
	game.Run();

	return 0;
}
