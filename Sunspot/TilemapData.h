#pragma once
#include <vector>
#include <string>
#include <nlohmann/json.hpp>

namespace Sunspot
{
	class TilemapData
	{
	private:

		using Json = nlohmann::json;

	public:

		explicit TilemapData(const std::string& filename);

		int width;
		int height;

		std::vector<int> tiles;
		std::string tileset;
	};
}
