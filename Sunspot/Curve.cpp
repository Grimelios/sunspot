#include "Curve.h"

namespace Sunspot
{
	const Curve::BinomialArray Curve::Binomials =
	{
		std::vector<int>({ 1, 4, 6, 4, 1 }),
		std::vector<int>({ 1, 5, 10, 10, 5, 1 }),
		std::vector<int>({ 1, 6, 15, 20, 15, 6, 1 })
	};

	Curve::Curve(std::vector<glm::vec2> controlPoints) : controlPoints(std::move(controlPoints))
	{
	}

	glm::vec2 Curve::Bezier(const float t, const std::vector<int>& binomials) const
	{
		glm::vec2 point(0);

		for (int i = 0; i <= degree; i++)
		{
			// See https://pomax.github.io/bezierinfo/.
			point += controlPoints[i] * static_cast<float>(binomials[i]) * std::pow(1 - t, degree - i) *
				std::pow(t, i);
		}

		return point;
	}
}
