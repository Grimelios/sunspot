#include "Game.h"
#include "Messaging.h"
#include <glm/vec2.hpp>
#include "Resolution.h"

namespace Sunspot
{
	Game::Game(const std::string& title, const int width, const int height) : inputProcessor(camera)
	{
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

		Resolution::Width = width;
		Resolution::Height = height;

		if (window == nullptr)
		{
			glfwTerminate();

			return;
		}

		glfwMakeContextCurrent(window);
		glfwSetWindowUserPointer(window, this);

		if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
		{
			glfwTerminate();

			return;
		}

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glViewport(0, 0, width, height);

		RegisterCallbacks();
	}

	void Game::RegisterCallbacks() const
	{
		glfwSetKeyCallback(window, KeyCallback);
		glfwSetCharCallback(window, CharCallback);
		glfwSetCursorPosCallback(window, CursorCallback);
		glfwSetMouseButtonCallback(window, ButtonCallback);
		glfwSetFramebufferSizeCallback(window, ResizeCallback);
	}

	void Game::KeyCallback(GLFWwindow* window, const int key, const int scancode, const int action, const int mods)
	{
		// Key can be negative in some cases (such as Alt + Printscreen).
		if (key == -1)
		{
			return;
		}

		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();

		switch (action)
		{
			case GLFW_PRESS: processor.OnKeyPress(key, mods); break;
			case GLFW_RELEASE: processor.OnKeyRelease(key); break;
		}
	}

	void Game::CharCallback(GLFWwindow* window, const unsigned int codepoint)
	{
	}

	void Game::CursorCallback(GLFWwindow* window, const double x, const double y)
	{
		const float mX = static_cast<float>(x);
		const float mY = static_cast<float>(y);

		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();
		processor.OnMouseMove(mX, mY);
	}

	void Game::ButtonCallback(GLFWwindow* window, const int button, const int action, const int mods)
	{
		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();

		switch (action)
		{
			case GLFW_PRESS: processor.OnMouseButtonPress(button); break;
			case GLFW_RELEASE: processor.OnMouseButtonRelease(button); break;
		}
	}

	void Game::ResizeCallback(GLFWwindow* window, const int width, const int height)
	{
		glViewport(0, 0, width, height);

		Resolution::Width = width;
		Resolution::Height = height;

		Messaging::Send(MessageTypes::Resize, glm::ivec2(width, height));
	}

	InputProcessor& Game::GetInputProcessor()
	{
		return inputProcessor;
	}

	void Game::Run()
	{
		while (!glfwWindowShouldClose(window))
		{
			const float time = static_cast<float>(glfwGetTime());
			const float target = 1.0f / TargetFramerate;

			// Using an accumulator helps limit FPS to the desired target.
			accumulator += time - previousTime;
			previousTime = time;

			const bool shouldUpdate = accumulator >= target;

			if (shouldUpdate)
			{
				glfwPollEvents();

				while (accumulator >= target)
				{
					Update(target);
					accumulator -= target;
				}

				Draw();

				glfwSwapBuffers(window);
			}
		}

		glfwTerminate();
	}
}
