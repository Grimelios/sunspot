#pragma once
#include "Component2D.h"
#include "Texture2D.h"
#include <string>
#include "Enumerations.h"

namespace Sunspot
{
	class Sprite : public Component2D
	{
	private:

		const Texture2D& texture;

		std::optional<Bounds> source;

		Sprite(const Texture2D& texture, const std::optional<Bounds>& source, Alignments alignment);

		void RecomputeOrigin();

	public:

		explicit Sprite(const std::string& filename, Alignments alignment = Alignments::Center);
		explicit Sprite(const Texture2D& texture, Alignments alignment = Alignments::Center);

		Sprite(const std::string& filename, const Bounds& source, Alignments alignment = Alignments::Center);
		Sprite(const Texture2D& texture, const Bounds& source, Alignments alignment = Alignments::Center);

		void Source(const std::optional<Bounds>& source);
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
