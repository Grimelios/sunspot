#include "Line.h"

namespace Sunspot
{
	Line::Line(const glm::vec2& start, const glm::vec2& end) :
		// Lines are a bit different from other shapes in that they don't really have a position (because it's a pair
		// of points).
		Shape(glm::vec2(0), 0, ShapeTypes::Line),

		start(start),
		end(end)
	{
	}
}
