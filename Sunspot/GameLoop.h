#pragma once
#include "IDynamic.h"
#include "IRenderable.h"

namespace Sunspot
{
	class GameLoop : public IDynamic, public IRenderable
	{
	protected:

		Camera& camera;
		SpriteBatch& sb;
		PrimitiveBatch& pb;

		GameLoop(Camera& camera, SpriteBatch& sb, PrimitiveBatch& pb);

	public:

		virtual void Initialize() = 0;
	};
}
