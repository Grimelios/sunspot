#pragma once
#include "Mechanism.h"
#include "TilemapRender.h"
#include <glm/vec2.hpp>
#include "ShipGateData.h"
#include "ShipWheel.h"

namespace Sunspot
{
	class ShipWheel;
	class ShipGate : public Mechanism
	{
	private:

		static const ShipGateData Data;

		bool open;

		TilemapRender render;

		glm::vec2 openPosition = glm::vec2(0);
		glm::vec2 closedPosition = glm::vec2(0);

		// This function is needed while moving (since calling the public function also sets open and close positions).
		void SetPositionInternal(const glm::vec2& position);

	protected:

		// Note that this function refers to the player trying to open the door manually. Actual opening and closing
		// is done through Toggle().
		void Activate(Player& player) override;

	public:

		ShipGate(const Json& j, Scene* scene);

		void Toggle();
		void SetPosition(const glm::vec2& position) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
