#pragma once
#include "BatchBase.h"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include "Bounds.h"
#include <glad.h>
#include "Shader.h"
#include "PrimitiveGroup.h"
#include "SpriteBatch.h"
#include "VectorUtilities.h"
#include <array>
#include "Line.h"

namespace Sunspot
{
	class SpriteBatch;
	class PrimitiveBatch : public BatchBase
	{
	private:

		SpriteBatch * sb = nullptr;
		Shader shader;

		GLint flipLocation = 0;
		GLint mvpLocation = 0;

		GLuint vao = 0;
		GLuint program = 0;
		GLuint positionBuffer = 0;
		GLuint colorBuffer = 0;
		GLuint indexBuffer = 0;

		PrimitiveGroup lineGroup;
		PrimitiveGroup pointGroup;
		PrimitiveGroup triangleGroup;

		std::array<int, 1> pointIndices;
		std::array<int, 2> lineIndices;
		std::array<int, 8> boundsOutlineIndices;
		std::array<int, 6> quadIndices;

		// Note that the index array is intentionally copied so that the index offset can be safely added to each element.
		template<int V, int I>
		void PushData(PrimitiveGroup& group, const std::array<glm::vec2, V>& vertexArray, const glm::vec4& color,
			std::array<int, I> indexArray);

		void Flush(BatchCoords coords) override;
		void Flush(PrimitiveGroup& group) const;

	public:

		explicit PrimitiveBatch(const Camera& camera);

		void SetOther(SpriteBatch* sb);

		// Flip needs to be modified within the shader to properly use render targets. This value is set externally
		// from the sprite batch.
		void Flip(int flip) const;
		void DrawPoint(const glm::vec2& point, const glm::vec4& color);
		void DrawLine(const Line& line, const glm::vec4& color);
		void DrawLine(const glm::vec2& start, const glm::vec2& end, const glm::vec4& color);
		void DrawBounds(const Bounds& bounds, const glm::vec4& color);
		void FillBounds(const Bounds& bounds, const glm::vec4& color);
	};

	template<int V, int I>
	void PrimitiveBatch::PushData(PrimitiveGroup& group, const std::array<glm::vec2, V>& vertexArray,
		const glm::vec4& color, std::array<int, I> indexArray)
	{
		std::vector<glm::vec3> vertices3D(V);

		for (int i = 0; i < V; i++)
		{
			vertices3D[i] = glm::vec3(vertexArray[i], zValue);
		}

		int& indexOffset = group.indexOffset;

		for (int& index : indexArray)
		{
			index += indexOffset;
		}

		VectorUtilities::PushVector(group.vertices, vertices3D);
		VectorUtilities::PushArray(group.indices, indexArray);
		VectorUtilities::Repeat(group.colors, color, V);

		indexOffset += V;

		zValue -= ZIncrement;
		sb->zValue = zValue;
	}
}
