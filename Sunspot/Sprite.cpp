#include "Sprite.h"
#include "GameFunctions.h"
#include "ContentCache.h"
#include "SpriteBatch.h"

namespace Sunspot
{
	Sprite::Sprite(const std::string& filename, const Alignments alignment) :
		Sprite(ContentCache::GetTexture(filename), std::nullopt, alignment)
	{
	}

	Sprite::Sprite(const Texture2D& texture, const Alignments alignment) :
		Sprite(texture, std::nullopt, alignment)
	{
	}

	Sprite::Sprite(const std::string& filename, const Bounds& source, const Alignments alignment) :
		Sprite(ContentCache::GetTexture(filename), std::optional<Bounds>(source), alignment)
	{
	}

	Sprite::Sprite(const Texture2D& texture, const Bounds& source, const Alignments alignment) :
		Sprite(texture, std::optional<Bounds>(source), alignment)
	{
	}

	Sprite::Sprite(const Texture2D& texture, const std::optional<Bounds>& source, const Alignments alignment) :
		Component2D(alignment),

		texture(texture),
		source(source)
	{
		RecomputeOrigin();
	}

	void Sprite::RecomputeOrigin()
	{
		origin = source == std::nullopt
			? GameFunctions::ComputeOrigin(texture.Width(), texture.Height(), alignment)
			: GameFunctions::ComputeOrigin(source->width, source->height, alignment);
	}

	void Sprite::Source(const std::optional<Bounds>& source)
	{
		this->source = source;

		RecomputeOrigin();
	}

	void Sprite::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		sb.Draw(texture, source, position, origin, color, rotation, scale, mods);
	}
}
