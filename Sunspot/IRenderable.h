#pragma once
#include "PrimitiveBatch.h"

namespace Sunspot
{
	class IRenderable
	{
	public:

		virtual ~IRenderable() = default;
		virtual void Draw(SpriteBatch& sb, PrimitiveBatch& pb) = 0;
	};
}
