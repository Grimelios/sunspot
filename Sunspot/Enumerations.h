#pragma once

namespace Sunspot
{
	enum class Alignments
	{
		Center = 0,
		Left = 1,
		Right = 2,
		Top = 4,
		Bottom = 8
	};

	// Putting this enumeration here (rather than in Component2D) simplifies linking with the SpriteBatch (specifically
	// in regards to IRenderable).
	enum class SpriteModifiers
	{
		None = 0,
		FlipVertical = 1,
		FlipHorizontal = 2
	};

	// Similar to other enumerations, keeping the enum here simplifies linking for sensors.
	enum class SensorTypes
	{
		Boss = 0,
		Character = 1,
		Enemy = 2,
		Hurtbox = 3,
		Mechanism = 4,
		Player = 5,
		World = 6,
		Count = 7
	};

	// Note that entity "types" are different than entity "groups" (which may or may not be added in order to
	// better organize entities).
	enum class EntityTypes
	{
		BigTentacle,
		Octoguard,
		ShipGate,
		ShipWheel,
		Tilemap
	};


	inline Alignments operator&(Alignments a1, Alignments a2)
	{
		return static_cast<Alignments>(static_cast<int>(a1) & static_cast<int>(a2));
	}

	inline Alignments operator|(Alignments a1, Alignments a2)
	{
		return static_cast<Alignments>(static_cast<int>(a1) | static_cast<int>(a2));
	}

	inline int operator&(const SpriteModifiers m1, const SpriteModifiers m2)
	{
		return static_cast<int>(m1) & static_cast<int>(m2);
	}

	inline SpriteModifiers operator|(SpriteModifiers m1, SpriteModifiers m2)
	{
		return static_cast<SpriteModifiers>(static_cast<int>(m1) | static_cast<int>(m2));
	}
}
