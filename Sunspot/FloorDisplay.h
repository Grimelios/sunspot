#pragma once
#include "HudElement.h"

namespace Sunspot
{
	class FloorDisplay : public HudElement
	{
	public:

		FloorDisplay();

		void SetLocation(const glm::ivec2& location) override;
		void Update(float dt) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
