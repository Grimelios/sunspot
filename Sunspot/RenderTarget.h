#pragma once
#include <glad.h>

namespace Sunspot
{
	class RenderTarget
	{
	private:

		int width;
		int height;

		GLuint textureId = 0;
		GLuint frameBuffer = 0;
		GLuint depthBuffer = 0;

	public:

		// This constructor is useful when render target size requires calculation.
		RenderTarget() = default;
		RenderTarget(int width, int height);

		int Width() const;
		int Height() const;

		GLuint TextureId() const;
		GLuint FrameBuffer() const;
		GLuint DepthBuffer() const;
	};
}
