#include "TilemapData.h"
#include "Paths.h"
#include "StringUtilities.h"
#include "JsonUtilities.h"

namespace Sunspot
{
	TilemapData::TilemapData(const std::string& filename)
	{
		Json j = JsonUtilities::Load("Tilemaps/" + filename);

		width = j.at("Width").get<int>();
		height = j.at("Height").get<int>();
		tileset = j.at("Tileset").get<std::string>();
		tiles.reserve(width * height);

		const std::string rawTiles = j.at("Tiles").get<std::string>();

		// This assumes that tiles will be listed in one long comma-separated string (no line breaks).
		for (const std::string& s : StringUtilities::Split(rawTiles, ','))
		{
			tiles.emplace_back(std::stoi(s));
		}
	}
}
