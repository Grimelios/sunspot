#pragma once
#include "EditorTool.h"
#include <glm/vec2.hpp>
#include "MouseData.h"
#include "Camera.h"

namespace Sunspot
{
	class CameraTool : public EditorTool
	{
	private:

		Camera& camera;

		glm::vec2 cameraAnchor = glm::vec2(0);
		glm::vec2 screenAnchor = glm::vec2(0);

	public:

		explicit CameraTool(Camera& camera);

		void HandleMouse(const MouseData& data) override;
	};
}
