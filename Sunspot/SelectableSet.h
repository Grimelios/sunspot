#pragma once
#include <vector>
#include "MouseData.h"
#include "ISelectable.h"

namespace Sunspot
{
	class SelectableSet
	{
	public:

		// Using a separate list of ISelectable pointers allows a single set to be used for different kinds of items
		// (even among multiple classes).
		std::vector<ISelectable*> items;

		// This variable is public so that it can be nullified in edge cases where the mouse moves to new bounds in a
		// single frame.
		ISelectable* hoveredItem = nullptr;

		void HandleMouse(const MouseData& data);
	};
}
