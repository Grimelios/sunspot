#pragma once
#include "EditorTool.h"
#include "Scene.h"
#include "Tilemap.h"

namespace Sunspot
{
	class TileTool : public EditorTool
	{
	private:

		// The tool is either in "tile mode" or "map mode". Tile mode modifies tiles within a tilemap. Map mode
		// modifies maps as a whole.
		bool creationActive = false;

		int tileSize = 32;

		// Bounds are stored as tiles, then scaled based on tile size for rendering.
		Bounds bounds;
		Tilemap* hoveredMap = nullptr;

		glm::ivec2 anchor;
		std::vector<Tilemap*> tilemaps;

	public:

		TileTool();

		void Reload(Scene& scene) override;
		void HandleMouse(const MouseData& data) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
