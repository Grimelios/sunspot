#pragma once

namespace Sunspot::Ease
{
	template<class T>
	T Linear(const T& start, const T& end, const float amount)
	{
		return start + (end - start) * amount;
	}
}
