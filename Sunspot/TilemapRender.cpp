#include "TilemapRender.h"
#include "JsonUtilities.h"
#include "MapUtilities.h"
#include "ContentCache.h"

namespace Sunspot
{
	TilemapRender::TilesetMap TilemapRender::tilesets;

	bool TilemapRender::tilesetsLoaded = false;

	// This function is only called once when the first tilemap is created. The assumption is that tileset data is
	// small enough that the full list of all tilesets can be easily kept in memory while the game runs. Note that the
	// actual tileset textures are still loaded and unloaded as usual.
	void TilemapRender::LoadTilesets()
	{
		const std::map<std::string, Json> map = JsonUtilities::Load("Tilesets.json");

		for (const auto& pair : map)
		{
			const Json j = pair.second;
			const std::string filename = j.at("Filename").get<std::string>();

			const int tileWidth = j.at("TileWidth").get<int>();
			const int tileHeight = j.at("TileHeight").get<int>();
			const int tileSpacing = j.at("TileSpacing").get<int>();

			MapUtilities::Add(tilesets, pair.first, Tileset(filename, tileWidth, tileHeight, tileSpacing));
		}
	}

	TilemapRender::TilemapRender(const std::string& filename) : TilemapRender(TilemapData(filename))
	{
	}

	TilemapRender::TilemapRender(const TilemapData& data) :
		width(data.width),
		height(data.height),
		tiles(data.tiles)
	{
		// Tilesets are only loaded once (when the first tilemap is created).
		if (!tilesetsLoaded)
		{
			LoadTilesets();

			tilesetsLoaded = true;
		}

		const Tileset& t = tilesets.at(data.tileset);
		tileset = &ContentCache::GetTexture("Tilesets/" + t.Filename());
		tileWidth = t.TileWidth();
		tileHeight = t.TileHeight();
		tileSpacing = t.TileSpacing();
		tilesPerRow = (tileset->Width() - tileSpacing) / (tileWidth + tileSpacing);

		bounds.width = width * tileWidth;
		bounds.height = height * tileHeight;
	}

	const Bounds& TilemapRender::GetBounds() const
	{
		return bounds;
	}

	void TilemapRender::SetPosition(const glm::vec2& position)
	{
		this->position = position;

		bounds.SetLocation(position);
	}

	void TilemapRender::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		glm::vec2 drawPosition = position;
		std::optional<Bounds> tileSource(Bounds(tileWidth, tileHeight));

		Bounds& tileBounds = tileSource.value();

		// C stands for "combined".
		const int cX = tileWidth + tileSpacing;
		const int cY = tileHeight + tileSpacing;

		for (int i = 0; i < height; i++)
		{
			const int start = i * width;

			drawPosition.x = position.x;

			for (int j = 0; j < width; j++)
			{
				const int tile = tiles[start + j];

				// -1 represents an empty tile.
				if (tile != -1)
				{
					tileBounds.x = (tile % tilesPerRow) * cX + tileSpacing;
					tileBounds.y = (tile / tilesPerRow) * cY + tileSpacing;

					sb.Draw(*tileset, tileSource, drawPosition);
				}

				drawPosition.x += tileWidth;
			}

			drawPosition.y += tileHeight;
		}
	}
}
