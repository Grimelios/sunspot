#pragma once
#include "IHoverable.h"

namespace Sunspot
{
	class ISelectable : public IHoverable
	{
	public:

		virtual ~ISelectable() = default;
		virtual void OnSelect() = 0;
	};
}
