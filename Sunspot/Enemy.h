#pragma once
#include "LivingEntity.h"
#include "EnemyHelper.h"

namespace Sunspot
{
	class Enemy : public LivingEntity
	{
	protected:

		virtual void AI(float dt);

	public:

		static const EnemyHelper Helper;

		void Update(float dt) override;
	};
}
