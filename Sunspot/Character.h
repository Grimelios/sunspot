#pragma once
#include "LivingEntity.h"

namespace Sunspot
{
	class Character : public LivingEntity
	{
	private:

		std::string name;

	protected:

		explicit Character(std::string name);
	};
}
