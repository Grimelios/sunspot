#include "PlayerData.h"

namespace Sunspot
{
	PlayerData::PlayerData()
	{
		Json j = Load("Player.json");

		sourceWidth = j.at("SourceWidth").get<int>();
		sourceHeight = j.at("SourceHeight").get<int>();
	}
}
