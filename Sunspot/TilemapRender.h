#pragma once
#include "IRenderable.h"
#include "Tileset.h"
#include "TilemapData.h"
#include <nlohmann/json.hpp>
#include <glm/vec2.hpp>

namespace Sunspot
{
	class TilemapData;

	// Using a separate class for tilemap rendering allows non-tilemaps to use tilemap-like rendering without the full
	// cost of creating a Tilemap entity.
	class TilemapRender : public IRenderable
	{
	private:

		using Json = nlohmann::json;
		using TilesetMap = std::map<std::string, Tileset>;

		static TilesetMap tilesets;
		static bool tilesetsLoaded;
		static void LoadTilesets();

		const Texture2D* tileset = nullptr;

		int width;
		int height;
		int tileWidth = 0;
		int tileHeight = 0;
		int tileSpacing = 0;
		int tilesPerRow = 0;

		std::vector<int> tiles;
		glm::vec2 position = glm::vec2(0);

		Bounds bounds;

	public:

		explicit TilemapRender(const std::string& filename);
		explicit TilemapRender(const TilemapData& data);

		const Bounds& GetBounds() const;

		void SetPosition(const glm::vec2& position);
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
