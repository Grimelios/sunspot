#pragma once
#include "Timer.h"
#include "DoublyLinkedList.h"

namespace Sunspot
{
	class Timer;
	class TimerCollection : public IDynamic
	{
	public:

		DoublyLinkedList<Timer> timers;

		void Update(float dt) override;
	};
}
