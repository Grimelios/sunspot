#pragma once
#include "DoublyLinkedListNode.h"

namespace Sunspot
{
	template<class T>
	class DoublyLinkedList
	{
	private:

		DoublyLinkedListNode<T>* head = nullptr;
		DoublyLinkedListNode<T>* tail = nullptr;

		int count = 0;

		DoublyLinkedListNode<T>* Add(DoublyLinkedListNode<T>* node);

	public:

		~DoublyLinkedList();

		template<class... Args>
		DoublyLinkedListNode<T>* Add(Args&&... args);
		DoublyLinkedListNode<T>* Add(const T& data);
		DoublyLinkedListNode<T>* Head() const;
		DoublyLinkedListNode<T>* Tail() const;
		
		void Remove(const DoublyLinkedListNode<T>* node);

		int Count() const;

		bool IsEmpty() const;
	};

	template <class T>
	DoublyLinkedList<T>::~DoublyLinkedList()
	{
		while (head != nullptr)
		{
			DoublyLinkedListNode<T>* temp = head;
			head = head->next;

			delete temp;
		}
	}

	template <class T>
	template <class... Args>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::Add(Args&&... args)
	{
		return Add(new DoublyLinkedListNode<T>(args...));
	}

	template <class T>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::Add(const T& data)
	{
		return Add(new DoublyLinkedListNode<T>(data));
	}

	template <class T>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::Add(DoublyLinkedListNode<T>* node)
	{
		if (count == 0)
		{
			head = node;
			tail = node;
		}
		else
		{
			node->previous = tail;
			tail->next = node;
			tail = node;
		}

		count++;

		return node;
	}

	template <class T>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::Head() const
	{
		return head;
	}

	template <class T>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::Tail() const
	{
		return tail;
	}

	template <class T>
	void DoublyLinkedList<T>::Remove(const DoublyLinkedListNode<T>* node)
	{
		if (count == 1)
		{
			delete head;

			head = nullptr;
			tail = nullptr;
		}
		else if (node == head)
		{
			head->next->previous = nullptr;
			head = head->next;

			delete node;
		}
		else if (node == tail)
		{
			tail->previous->next = nullptr;
			tail = tail->previous;

			delete node;
		}
		else
		{
			node->previous->next = node->next;
			node->next->previous = node->previous;

			delete node;
		}

		count--;
	}

	template <class T>
	int DoublyLinkedList<T>::Count() const
	{
		return count;
	}

	template <class T>
	bool DoublyLinkedList<T>::IsEmpty() const
	{
		return count == 0;
	}
}
