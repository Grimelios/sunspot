#include "SavePanel.h"

namespace Sunspot
{
	SavePanel::SavePanel()
	{
		textbox.container = &container;
		container.textbox = &textbox;
	}

	void SavePanel::Location(const glm::ivec2& location)
	{
		container.Location(location);
	}

	void SavePanel::HandleKeyboard(const KeyboardData& data)
	{
		textbox.HandleKeyboard(data);
	}

	void SavePanel::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		container.Draw(sb, pb);
	}
}
