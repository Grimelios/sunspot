#pragma once
#include "DoublyLinkedList.h"
#include "Sensor.h"
#include <array>
#include "IDynamic.h"
#include "Enumerations.h"
#include "ShapeHelper.h"

namespace Sunspot
{
	class Sensor;
	class SensorWorld : public IDynamic
	{
	private:

		// This helper will likely be replaced with a static one in Shape.
		ShapeHelper helper;

	public:
		
		// These aliases are public for use in other classes (like the visualizer).
		using SensorNode = DoublyLinkedListNode<Sensor*>;
		using SensorList = DoublyLinkedList<Sensor*>;
		using SensorArray = std::array<SensorList, static_cast<int>(SensorTypes::Count)>;

		const SensorArray& Sensors() const;

		void Add(Sensor* sensor);
		void Remove(Sensor* sensor);
		void Update(float dt) override;

	private:

		SensorArray sensors;
	};
}
