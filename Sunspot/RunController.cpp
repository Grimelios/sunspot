#include "RunController.h"

namespace Sunspot
{
	RunController::RunController(Entity* parent) : parent(parent)
	{
	}

	void RunController::MaxSpeed(const int maxSpeed)
	{
		this->maxSpeed = maxSpeed;

		maxSpeedSquared = maxSpeed * maxSpeed;
	}

	void RunController::Update(const float dt)
	{
		parent->velocity = direction * static_cast<float>(maxSpeed);
	}
}
