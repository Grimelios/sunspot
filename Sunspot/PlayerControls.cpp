#include "PlayerControls.h"
#include "JsonUtilities.h"
#include <map>

namespace Sunspot
{
	PlayerControls::PlayerControls() 
	{
		std::map<std::string, Json> binds = JsonUtilities::Load("Controls/PlayerControls.json");

		up = ParseBinds(binds.at("Up"));
		down = ParseBinds(binds.at("Down"));
		left = ParseBinds(binds.at("Left"));
		right = ParseBinds(binds.at("Right"));
		attack = ParseBinds(binds.at("Attack"));
		interact = ParseBinds(binds.at("Interact"));
	}

	std::vector<InputBind> PlayerControls::ParseBinds(const Json& j)
	{
		std::vector<Json> rawBinds = j;
		std::vector<InputBind> binds;
		binds.reserve(rawBinds.size());

		for (const Json& b : rawBinds)
		{
			const InputTypes inputType = b.at("InputType").get<InputTypes>();
			const std::any data = b.at("Data").get<int>();

			binds.emplace_back(inputType, data);
		}

		return binds;
	}
}
