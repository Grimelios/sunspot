#pragma once
#include "Enumerations.h"

namespace Sunspot
{
	class ISensitive
	{
	public:

		virtual ~ISensitive() = default;
		virtual void OnSense(SensorTypes type, ISensitive* data) = 0;
		virtual void OnSeparation(SensorTypes type, ISensitive* data) = 0;
	};
}
