#pragma once
#include <optional>
#include "CharacterData.h"
#include "Texture2D.h"

namespace Sunspot
{
	class SpriteFont
	{
	public:

		// The value of 127 covers common English characters, numbers, and punctuation.
		static const int CharacterRange = 127;

		// Using std::optional is required since not all characters within the range will have data defined.
		using CharacterArray = std::array<std::optional<CharacterData>, CharacterRange>;

		SpriteFont(const Texture2D& texture, int size, const CharacterArray& dataArray);

		const CharacterArray& DataArray() const;
		const Texture2D& Texture() const;

		int Size() const;

		glm::ivec2 Measure(const std::string& value, bool measureLiteral = false) const;

	private:

		const Texture2D& texture;
		const CharacterArray dataArray;

		int size;
	};
}
