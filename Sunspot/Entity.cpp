#include "Entity.h"
#include "Scene.h"
#include "Parse.h"

namespace Sunspot
{
	void Entity::Initialize(const Json& j)
	{
		// This assumes that all entities that call this function contain position.
		SetPosition(Parse::Vec2(j.at("Position").get<std::string>()));

		// Most entities remain axis-aligned (not rotated).
		const auto i = j.find("Rotation");

		if (i != j.end())
		{
			SetRotation(i->get<float>());
		}
	}

	glm::vec2 Entity::GetPosition() const
	{
		return position;
	}

	float Entity::GetRotation() const
	{
		return rotation;
	}

	const Bounds& Entity::GetRenderBounds() const
	{
		return renderBounds;
	}

	void Entity::SetPosition(const glm::vec2& position)
	{
		this->position = position;

		for (Component2D* c : components)
		{
			c->position = position;
		}

		// Shape can be null in rare cases (such as a sword being held, but not swung).
		if (shape != nullptr)
		{
			shape->position = position + shapeOffset;
		}
	}

	void Entity::SetRotation(const float rotation)
	{
		this->rotation = rotation;

		for (Component2D* c : components)
		{
			c->rotation = rotation;
		}
	}

	void Entity::OnSense(const SensorTypes type, ISensitive* data)
	{
	}

	void Entity::OnSeparation(const SensorTypes type, ISensitive* data)
	{
	}

	void Entity::Dispose()
	{
		scene->Remove(this);
	}

	void Entity::Update(const float dt)
	{
		timerCollection.Update(dt);
		
		SetPosition(position + velocity * dt);
	}

	void Entity::Draw(SpriteBatch& sb, PrimitiveBatch& pb)
	{
		for (Component2D* c : components)
		{
			c->Draw(sb, pb);
		}
	}
}
