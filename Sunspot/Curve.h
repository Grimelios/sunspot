#pragma once
#include <array>
#include <vector>
#include <glm/vec2.hpp>

namespace Sunspot
{
	class Curve
	{
	private:

		// This array can easily be expanded if curves with a greater number of control points are needed. Note that
		// quadratic and cubic curves have separate, optimized functions, so the first vector in this array actually
		// stores values for curves with n = 5 control points.
		using BinomialArray = std::array<std::vector<int>, 3>;

		static const BinomialArray Binomials;

		// Note that degree is always one less than the number of control points (e.g. quadratic curves have three
		// points).
		int degree = 4;

		glm::vec2 Bezier(float t, const std::vector<int>& binomials) const;

	public:

		// Curves are assumed to have at least three control points (or else they'd just be points or lines).
		Curve() = default;
		explicit Curve(std::vector<glm::vec2> controlPoints);

		std::vector<glm::vec2> controlPoints;

		template<int S>
		std::array<glm::vec2, S> Evaluate() const;
	};

	template <int S>
	std::array<glm::vec2, S> Curve::Evaluate() const
	{
		std::array<glm::vec2, S> points;

		const std::vector<int>& binomials = Binomials[degree - 4];

		for (int i = 0; i < S; i++)
		{
			const float t = i / (S - 1.0f);

			// Again, see the link above.
			points[i] = Bezier(t, binomials);
		}

		return points;
	}
}
