#include "SpriteFont.h"

namespace Sunspot
{
	SpriteFont::SpriteFont(const Texture2D& texture, const int size, const CharacterArray& dataArray) :
		texture(texture),
		dataArray(dataArray),
		size(size)
	{
	}

	const SpriteFont::CharacterArray& SpriteFont::DataArray() const
	{
		return dataArray;
	}

	const Texture2D& SpriteFont::Texture() const
	{
		return texture;
	}

	int SpriteFont::Size() const
	{
		return size;
	}

	glm::ivec2 SpriteFont::Measure(const std::string& value, const bool measureLiteral) const
	{
		if (value.empty())
		{
			return glm::ivec2(0, size);
		}

		int sumWidth = 0;
		const int length = static_cast<int>(value.size());

		for (int i = 0; i < length - 1; i++)
		{
			sumWidth += dataArray[value[i]]->Advance();
		}

		sumWidth += dataArray[value[length - 1]]->Width();

		return glm::ivec2(sumWidth, size);
	}
}
