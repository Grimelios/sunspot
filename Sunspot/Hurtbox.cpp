#include "Hurtbox.h"

namespace Sunspot
{
	Hurtbox::Hurtbox(Shape* shape, DamageSource* source) :
		sensor(SensorTypes::Hurtbox, shape, this),
		source(source)
	{
		sensor.canActivate.push_back(SensorTypes::Enemy);
	}

	void Hurtbox::OnSense(const SensorTypes sensorType, ISensitive* data)
	{
		const auto target = reinterpret_cast<ITargetable*>(data);

		ApplyHit(this->source->Damage(), this->source->Knockback(), target);

		// This assumes that each attack can only hit targets once. This could be updated in the future to account
		// for continual damage (such as poison clouds).
		targetsHit.push_back(target);
	}

	void Hurtbox::OnSeparation(const SensorTypes sensorType, ISensitive* data)
	{
	}

	void Hurtbox::Dispose()
	{
		sensor.Dispose();
	}
}
