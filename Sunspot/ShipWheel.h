#pragma once
#include "Mechanism.h"
#include "ShipGate.h"
#include "ShipWheelData.h"
#include "EntityHandle.h"
#include "Sprite.h"

namespace Sunspot
{
	class ShipGate;
	class ShipWheel : public Mechanism
	{
	private:
		
		friend class ShipGate;

		Sprite sprite;
		std::vector<EntityHandle> gateHandles;

	protected:

		static const ShipWheelData Data;

		void Activate(Player& player) override;

	public:

		ShipWheel(const Json& j, Scene* scene);
	};
}
