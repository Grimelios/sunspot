#include "Tileset.h"

namespace Sunspot
{
	Tileset::Tileset(std::string filename, const int tileWidth, const int tileHeight, const int tileSpacing) :
		filename(std::move(filename)),
		tileWidth(tileWidth),
		tileHeight(tileHeight),
		tileSpacing(tileSpacing)
	{
	}

	const std::string& Tileset::Filename() const
	{
		return filename;
	}

	int Tileset::TileWidth() const
	{
		return tileWidth;
	}

	int Tileset::TileHeight() const
	{
		return tileHeight;
	}

	int Tileset::TileSpacing() const
	{
		return tileSpacing;
	}
}
