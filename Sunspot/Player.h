#pragma once
#include "LivingEntity.h"
#include "Sprite.h"
#include "PlayerControls.h"
#include "AggregateData.h"
#include "RunController.h"
#include "Sword.h"

namespace Sunspot
{
	class Player : public LivingEntity
	{
	private:

		Sword sword;
		Sprite sprite;
		PlayerControls controls;
		RunController runController;

		void HandleInput(const AggregateData& data, float dt);

	public:

		Player();

		void Update(float dt) override;
	};
}
