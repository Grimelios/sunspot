#pragma once
#include <glm/vec2.hpp>
#include <array>

namespace Sunspot
{
	class Bounds
	{
	public:

		Bounds();
		Bounds(int width, int height);
		Bounds(int x, int y, int width, int height);
		Bounds(const glm::ivec2& location, int width, int height);

		int x;
		int y;
		int width;
		int height;

		int Left() const;
		int Right() const;
		int Top() const;
		int Bottom() const;

		glm::ivec2 GetLocation() const;
		glm::ivec2 GetCenter() const;

		void SetLocation(const glm::ivec2& location);
		void SetCenter(const glm::ivec2& center);

		void Left(int left);
		void Right(int right);
		void Top(int top);
		void Bottom(int bottom);

		bool Contains(const glm::vec2& position) const;

		std::array<glm::vec2, 4> GetCorners() const;

		Bounds Scale(int scalar) const;
	};
}
