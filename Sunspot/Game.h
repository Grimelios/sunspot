#pragma once
#include <glad.h>
#include <glfw3.h>
#include <string>
#include "InputProcessor.h"

namespace Sunspot
{
	class Game
	{
	private:

		static const int TargetFramerate = 60;

		static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void CharCallback(GLFWwindow* window, unsigned int codepoint);
		static void CursorCallback(GLFWwindow* window, double x, double y);
		static void ButtonCallback(GLFWwindow* window, int button, int action, int mods);
		static void ResizeCallback(GLFWwindow* window, int width, int height);

		GLFWwindow* window;

		float previousTime = 0;
		float accumulator = 0;

		void RegisterCallbacks() const;

	protected:

		Camera camera;
		InputProcessor inputProcessor;

		Game(const std::string& title, int width, int height);

		virtual void Update(float dt) = 0;
		virtual void Draw() = 0;

	public:

		virtual ~Game() = default;
		virtual void Initialize() = 0;

		// This function is required for static GLFW callback functions.
		InputProcessor& GetInputProcessor();

		void Run();
	};
}
