#pragma once
#include "Container2D.h"
#include "Sprite.h"
#include <array>
#include "Editor.h"
#include "EditorToolbarIcon.h"
#include <memory>
#include "SelectableSet.h"

namespace Sunspot
{
	class Editor;
	class EditorToolbarIcon;
	class EditorToolbar : public Container2D
	{
	private:

		using IconArray = std::array<std::unique_ptr<EditorToolbarIcon>, 2>;

		IconArray icons;
		Editor* parent;

		int activeIndex = 0;

	public:

		EditorToolbar(SelectableSet& parentSet, Editor* parent);

		void SetActive(int index);
		void SetLocation(const glm::ivec2& location) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
