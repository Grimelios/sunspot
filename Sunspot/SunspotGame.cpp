#include "SunspotGame.h"
#include "GameplayLoop.h"
#include "Messaging.h"
#include "Resolution.h"

namespace Sunspot
{
	SunspotGame::SunspotGame(const int width, const int height) : Game("Sunspot", width, height),
		sb(camera),
		pb(camera)
	{
		glClearColor(0, 0, 0, 1);

		sb.SetOther(&pb);
		pb.SetOther(&sb);
	}

	void SunspotGame::Initialize()
	{
		gameLoop = std::make_unique<GameplayLoop>(camera, sb, pb);
		gameLoop->Initialize();

		Bounds& target = Resolution::Target;
		camera.origin = glm::ivec2(target.width, target.height) / 2;

		Messaging::Send(MessageTypes::Resize, glm::ivec2(Resolution::Width, Resolution::Height));
	}

	void SunspotGame::Update(const float dt)
	{
		inputProcessor.Update(dt);
		gameLoop->Update(dt);
		camera.Update(dt);
	}

	void SunspotGame::Draw()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		gameLoop->Draw(sb, pb);
		pb.ResetZ();
		sb.ResetZ();
	}
}
