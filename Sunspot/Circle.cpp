#include "Circle.h"

namespace Sunspot
{
	Circle::Circle() : Circle(0, glm::vec2(0))
	{
	}

	Circle::Circle(const float radius) : Circle(radius, glm::vec2(0))
	{
	}

	Circle::Circle(const float radius, const glm::vec2& center) :
		Shape(center, 0, ShapeTypes::Circle),

		radius(radius)
	{
	}
}
