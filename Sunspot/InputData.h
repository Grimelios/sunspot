#pragma once
#include <any>

namespace Sunspot
{
	enum class InputStates
	{
		Held = 1,
		Released = 4,
		PressedThisFrame = 3,
		ReleasedThisFrame = 12
	};

	enum class InputTypes
	{
		Keyboard = 0,
		Mouse = 1,
		Count = 2
	};

	class InputData
	{
	public:

		virtual ~InputData() = default;
		virtual bool Query(const std::any& data, InputStates state) const = 0;
	};

	inline InputStates operator&(InputStates s1, InputStates s2)
	{
		return static_cast<InputStates>(static_cast<int>(s1) & static_cast<int>(s2));
	}
}
