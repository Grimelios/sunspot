#pragma once
#include "EditorPanel.h"
#include "Textbox.h"
#include "EditorTextboxContainer.h"

namespace Sunspot
{
	class SavePanel : public EditorPanel
	{
	private:

		Textbox textbox;
		EditorTextboxContainer container;

	public:

		SavePanel();

		void Location(const glm::ivec2& location) override;
		void HandleKeyboard(const KeyboardData& data) override;
		void Draw(SpriteBatch& sb, PrimitiveBatch& pb) override;
	};
}
