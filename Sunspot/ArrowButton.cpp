#include "ArrowButton.h"
#include "EditorConstants.h"

namespace Sunspot
{
	ArrowButton::ArrowButton(const Texture2D& texture, const Bounds& source, const bool flipped) :
		Button(texture, source)
	{
		icon.Opacity(EditorConstants::InactiveOpacity);
		bounds.width = EditorConstants::IconSize;
		bounds.height = EditorConstants::IconSize;

		if (flipped)
		{
			icon.mods = SpriteModifiers::FlipHorizontal;
		}
	}

	void ArrowButton::OnHover()
	{
		icon.Opacity(1);
	}

	void ArrowButton::OnUnhover()
	{
		icon.Opacity(EditorConstants::InactiveOpacity);
	}
}
