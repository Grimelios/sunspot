#pragma once
#include <glm/vec2.hpp>
#include "InputData.h"
#include <glad.h>
#include <glfw3.h>
#include <array>

namespace Sunspot
{
	enum class MouseButtons
	{
		Left,
		Right,
		Middle
	};

	class MouseData : public InputData
	{
	private:

		using ButtonArray = std::array<InputStates, GLFW_MOUSE_BUTTON_LAST>;

		glm::vec2 screenPosition;
		glm::vec2 worldPosition;
		glm::vec2 previousScreenPosition;
		glm::vec2 previousWorldPosition;

		ButtonArray buttonArray;

	public:

		MouseData(const glm::vec2& screenPosition, const glm::vec2& worldPosition, const glm::vec2& previousScreenPosition,
			const glm::vec2& previousWorldPosition, const ButtonArray& buttonArray);

		glm::vec2 ScreenPosition() const;
		glm::vec2 WorldPosition() const;
		glm::vec2 PreviousScreenPosition() const;
		glm::vec2 PreviousWorldPosition() const;

		InputStates State(int button) const;

		bool Query(const std::any& data, InputStates state) const override;
	};
}
