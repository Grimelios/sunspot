#pragma once
#include "Camera.h"
#include <glm/mat4x4.hpp>

namespace Sunspot
{
	enum class BatchCoords
	{
		Screen,
		World,
		Unassigned
	};

	class BatchBase
	{
	private:

		BatchCoords coords = BatchCoords::Unassigned;

	protected:

		// Z-sorting is implemented using 3D coordinates. For each draw call, the current Z value is decremented by a
		// small amount, then reset on flush. Note that the Z value is only reset to its maximum value once at the
		// beginning of each frame (since sprites and primitives should continue to be sorted even through restarts).
		static const float ZStart;
		static const float ZIncrement;

		const Camera& camera;

		glm::mat4 projection;
		glm::mat4 screenMatrix;

		bool drawing = false;

		explicit BatchBase(const Camera& camera);

		virtual void Flush(BatchCoords coords) = 0;

	public:

		virtual ~BatchBase();

		float zValue = ZStart;

		void Begin(BatchCoords coords);
		void End();

		// Batches can be flushed multiple times per frame if render targets are used, but the Z value should only be reset
		// once per frame.
		void ResetZ();
	};

	inline BatchBase::~BatchBase() = default;
}
