#pragma once
#include "InputData.h"

namespace Sunspot
{
	class InputBind
	{
	public:

		InputBind(InputTypes type, std::any data);

		InputTypes type;

		std::any data;
	};
}
